package org.literacyapp.familiar_word_reading;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import org.literacyapp.contentprovider.ContentProvider;
import org.literacyapp.contentprovider.model.content.Word;
import org.literacyapp.contentprovider.model.content.multimedia.Audio;
import org.literacyapp.contentprovider.util.MultimediaHelper;
import org.literacyapp.model.enums.content.SpellingConsistency;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class LetterSoundActivity extends AppCompatActivity  {

    //private List<Word> words;
    ArrayList<String> newWords;
    private List<Word> wordsSeen;
    int i=-1;
    private TextToSpeech tts;

    private TextView word1TextView;
    private TextView word2TextView;
    private TextView word3TextView;

    private ImageButton nextButton;
    MediaPlayer mp ;
    int soundCounter = 0;

    ArrayList<Integer> sounds  = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {



        Log.i(getClass().getName(), "onCreate");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_letter_sound);

        wordsSeen = new ArrayList<>();
//        tts = new TextToSpeech(this, this);

        word1TextView = (TextView) findViewById(R.id.word1TextView);
        word2TextView = (TextView) findViewById(R.id.word2TextView);
        word3TextView = (TextView) findViewById(R.id.word3TextView);

        nextButton = (ImageButton) findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(getClass().getName(), "nextButton onClick");

                loadNextWord();
            }
        });

        List<Word> wordsWithPerfectSpelling = new ArrayList<Word>() ;
        //addSounds();
        newWords  = new ArrayList<>();
        newWords.add("തവള");
        newWords.add("കടൽ");
        newWords.add("മണൽ");
        newWords.add("ചവണ");
        //newWords.add("പറവ ");

      /*  newWords.add("our");
        newWords.add("you");
        newWords.add("new");*/

                //ContentProvider.getAllWords(SpellingConsistency.PERFECT);
      //  Log.i(getClass().getName(), "wordsWithPerfectSpelling.size(): " + wordsWithPerfectSpelling.size());

        // TODO: dynamically fetch words that only contain the current student's unlocked letters (& syllables)
      //  words = new ArrayList<>();
        for (Word word : wordsWithPerfectSpelling) {
            //Log.i(getClass().getName(), "word.getText(): " + word.getText() + ", word.getPhonetics(): " + word.getPhonetics() + ", word.getSpellingConsistency(): " + word.getSpellingConsistency());

            // Skip if corresponding Audio is missing
           // Audio audio = ContentProvider.getAudio(word.getText());
           /* if (audio == null) {
                continue;
            }*/

            // TODO: dynamically start with shorter words, then gradually increase length
            if (word.getText().length() == 3) {
            //    words.add(word);
            }

          //  if (words.size() == 10) {
          //      break;
          //  }
        }
       // Log.i(getClass().getName(), "words.size(): " + words.size());
    }

    @Override
    protected void onStart() {
        Log.i(getClass().getName(), "onStart");
        super.onStart();

        loadNextWord();
    }

    private void loadNextWord() {
        Log.i(getClass().getName(), "loadNextWord");

        i++;
      int size = newWords.size()-1;
      if(size==0){
          finish();
      }

       /* if (wordsSeen.size() == words.size()) {
            // TODO: show congratulations page
            finish();
            return;
        }*/

        word1TextView.setVisibility(View.INVISIBLE);
        word2TextView.setVisibility(View.INVISIBLE);
        word3TextView.setVisibility(View.INVISIBLE);
        nextButton.setVisibility(View.INVISIBLE);

       /* final Word currentWord = words.get(wordsSeen.size());
        Log.i(getClass().getName(), "currentWord.getText(): " + currentWord.getText());*/

        word1TextView.postDelayed(new Runnable() {
            @Override
            public void run() {
                final String letter1 = newWords.get(i).substring(0, 1);
                Log.i(getClass().getName(), "letter1: " + letter1);
                word1TextView.setText(letter1);

                final String letter2 = newWords.get(i).substring(1, 2);
                Log.i(getClass().getName(), "letter2: " + letter2);
                word2TextView.setText(letter2);

                final String letter3 = newWords.get(i).substring(2, 3);
                Log.i(getClass().getName(), "letter3: " + letter3);
                word3TextView.setText(letter3);

                word1TextView.setVisibility(View.VISIBLE);
                word1TextView.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        playLetterSound(letter1);


                        word2TextView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                word2TextView.setVisibility(View.VISIBLE);
                                word2TextView.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        playLetterSound(letter2);


                                        word3TextView.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                word3TextView.setVisibility(View.VISIBLE);
                                                word3TextView.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {

                                                        playLetterSound(letter3);


                                                        word3TextView.postDelayed(new Runnable() {
                                                            @Override
                                                            public void run() {

                                                                playWord(newWords.get(i));
                                                               // wordsSeen.add(currentWord);
                                                                nextButton.setVisibility(View.VISIBLE);
                                                            }
                                                        }, 2000);
                                                    }
                                                }, 1000);
                                            }
                                        }, 2000);
                                    }
                                }, 1000);
                            }
                        }, 2000);
                    }
                }, 1000);
            }
        }, 1000);
    }


    private void playLetterSound(String letter) {

      switch (letter){
          case "ത" : mp = MediaPlayer.create(this,R.raw.thathavala);mp.start();break;
          case "വ" : mp = MediaPlayer.create(this,R.raw.vathavala);mp.start();break;
          case "ള" : mp = MediaPlayer.create(this,R.raw.lathavala);mp.start();break;

          case "ക" : mp = MediaPlayer.create(this,R.raw.kakadal);mp.start();break;
          case "ട" : mp = MediaPlayer.create(this,R.raw.dakadal);mp.start();break;
          case "ൽ" : mp = MediaPlayer.create(this,R.raw.ilkadal);mp.start();break;

          case "പ" : mp = MediaPlayer.create(this,R.raw.paparava);mp.start();break;
          case "റ" : mp = MediaPlayer.create(this,R.raw.raparava);mp.start();break;
         // case "വ" : mp = MediaPlayer.create(this,R.raw.vaparava);mp.start();break;

          case "ച" : mp = MediaPlayer.create(this,R.raw.chachavana);mp.start();break;
         // case "വ" : mp = MediaPlayer.create(this,R.raw.vachavana);mp.start();break;
         // case "ണ" : mp = MediaPlayer.create(this,R.raw.nachavana);mp.start();break;

          case "മ" : mp = MediaPlayer.create(this,R.raw.mamanal);mp.start();break;
          case "ണ" : mp = MediaPlayer.create(this,R.raw.namanal);mp.start();break;
        //  case "ൽ" : mp = MediaPlayer.create(this,R.raw.ilkadal);mp.start();break;


      }



    }

    private void playWord(String word) {

        switch (word){
            case "തവള" : mp = MediaPlayer.create(this,R.raw.thavala);mp.start();break;
            case "കടൽ" : mp = MediaPlayer.create(this,R.raw.kadal);mp.start();break;
            case "പറവ" : mp = MediaPlayer.create(this,R.raw.parava);mp.start();break;
            case "ചവണ" : mp = MediaPlayer.create(this,R.raw.chavana);mp.start();break;
            case "മണൽ" : mp = MediaPlayer.create(this,R.raw.manal);mp.start();break;

        }

    }

  /*  private void addSounds(){
            sounds.add(R.raw.tha);
        sounds.add(R.raw.va_for_thavala);
        sounds.add(R.raw.la_for_thavala);
        sounds.add(R.raw.thavala);


    }*/

   /* @Override
    public void onInit(int status) {

        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.UK);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {


            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }*/



}
