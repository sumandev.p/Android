$(document).ready(function() {
   menuclicked=0;
   shapeslink=0;
   designlink=0;
    // defining shape, color and pics for a Shape
    shapes=[{"shape":"circle","color":"#569482","pics":["nest","owl","frogeyes","cherry","tomato","beanseed"]},{"shape":"oval","color":"#ffcf99","pics":["watermelon","egg","badam","grapes","turtle","ladybug"]},{"shape":"square","color":"#ffb365","pics": ["clock","chessboard","bread","dice"] },{"shape":"rectangle","color":"#ffa3fa","pics":["photoframe","door","table","board","tablet"]},{"shape":"triangle","color":"#83c1ef","pics":["bogain","mountain","shell","sharkfin","pizzaslice","dogear"]},{"shape":"diamond","color":"lightsteelblue","pics":["kite","pineapple","stingray","fish"]},{"shape":"star","color":"#C799EC","pics":["starfish","starfruit","anise","mosanda","sunflower","starfinal"]},{"shape":"ellipse","color":"#AFDEB6","pics":["penguin","corn","americanfootball","feather","pebbles","avocado"]},{"shape":"curve","color":"snow","pics":["moon","banana","cattail","orange","horn","curvefinal"]},{"shape":"line","color":"#E0EF98","pics":["bamboo","drum","celery","pencil","agarbati","cocunutleaf"]}/*,{"shape":"crumpled","color":"#ffe9d9","pics":["walnut","treebark","sand","puppies","mushroom","chilly"]},{"shape":"furry","color":"#E4D7D7","pics":["cow","worm","milkweed","bud","cat","grass"]},{"shape":"sharp","color":"#95E66C","pics":["aloevera","thorn","woodpecker","jackfruit","chestnut","porcupine","sharpfinal"]}, {"shape":"broad","color":"#BCEA97","pics":["lotusleaf","turtleb","foot","ear","flight","stingrayb"]}*/];

    // defining design for a particular Shape
    designs=[slideDesign, bookDesign, fadeInRotate, blindFall,scaleFade,scaleZoom,blindUncover,slideDesign,slideFade,blindFall,puffTransfer,scaleZoom,slideFade,explodeShow];

    $("#shapesaudio").trigger("play");
    //$("#bg").trigger("play");
    $(".pages, #pallet").hide();
    $("#homepage").show();

    function randOrd() {
        return (Math.round(Math.random()) - 0.5);
    }
    $("a#activitylink").click(function(){
      shapeslink=0;
      designlink=0;
      if(menuclicked==0){
      $(".dropdown").addClass("show");
      $(this).next().toggle(500);
      $(".dropright .dropdown-menu").hide(500);
      $(".dropdown-submenu").css("background","none");
      }
    });
    $(".dropright .dropdown-submenu").click(function(){
    
    	$(".dropright .dropdown-menu").hide();
        context=$(this);
        linkclicked=$(this).attr("id");
        if( linkclicked== "shapesmenu"){
           shapeslink++;
           designlink=0;
          shapemode= shapeslink%2;
		if (shapemode==0){
		 $(".dropright .dropdown-menu").hide();
		}else{
		$("#designmenu").next().hide();
		$(".dropdown-submenu").css("background","none");
		context.css("background","gainsboro");
		context.next().fadeIn(300);
		}
        }else if( linkclicked== "designmenu"){
        	designlink++;
        	shapeslink=0;
        	designmode= designlink%2;
        	if (designmode==0){
		 $(".dropright .dropdown-menu").hide();
		}else{
		$("#shapesmenu").next().hide();
		$(".dropdown-submenu").css("background","none");
		context.css("background","gainsboro");
		context.next().fadeIn(300);
		}
        }
        
      /*  if( linkclicked== "designmenu"){
           designlink++;
           shapeslink=0;
           designmode= designlink%2;
           shapemode= shapeslink%2;
        }
        
	
	if (designmode==0){
         $("#shapesmenu").next().hide();
        }else{
        $(".dropdown-submenu").css("background","none");
        context.css("background","gainsboro");
	context.next().fadeIn(200);
	}*/
			
    });
    function findIndex(){
     indexValue=0;
     shapes.forEach(function(item, index){
      if(item.shape==currentShape){
        indexValue= index;
      }
     });
    }
    /*************************/
    $(document).on("click",".home",function() {
        location.reload();
    }); // homelink

    $(".menudesign, .menushape").click(function() {
      menuclicked++;
      $("#activitylink").next().hide();
      $(".dropdown").removeClass("show");
 	$("#activitylink").addClass("disabled");
      // hiding all images in the shape-design page
      $(".sdpage img, #pallet").hide();

      //finding the current Shape
      currentShape = $(this).attr("id");
      findIndex();
      colorValue =shapes[indexValue].color;

      // homepage+ bg-container slide
      $("#bg-container").hide('slide', {direction: 'left'}, 1000,function(){
        //showing the current page with cuurentshape, its bacgroundcolor, Design
        showPage(currentShape,colorValue, designs[indexValue]);
      });
    }); // homelink

    /************************************************home end****************************************************/
    function showPage(currentShape,colorvalue,design) {
            // presetting pages, button, images- background,pallet
          	$(".pages,.nextpagebutton,.pages img").hide();
          	$("#bg-container").css("background",colorvalue);
          	$("#pallet").css("background","url(images/"+currentShape+"/"+currentShape+"pallet.png)").show();
          	//$("#pallet").show();

            //showing bg container and the currentShape page
          	$("#bg-container").fadeIn(1000,function(){
                $("#"+currentShape+"page").show();
                $("#"+currentShape+"audio").trigger("play");
                  // Particular design for the shape
                	design(currentShape);
            });
    } //showpage id

    function slideDesign(currentShape){
      // removing any srtyle attr and resetting img position
      $("#"+currentShape+"page").addClass("slideDesign");
      $('.slideDesign .row img').removeAttr("style");
      $('.slideDesign .row img').css({
      'left': '-105%',
      "position":"relative",
      'opacity':'0'
      });

      //Sliding images
      timeGap=2000;
      for(img_count=0; img_count< shapes[indexValue].pics.length; img_count++){
       var show_image = anime({
                 targets: "#"+shapes[indexValue].pics[img_count],
                 opacity: 1,
                 left: '0',
                 delay:timeGap*(img_count+1),
                 easing: 'linear',
                 duration: 1000
               });
               //var promise = show_image.finished.then(show_image);
             }

        // exploding and showing all images in currentshape
        $("#"+currentShape+"page .row img").delay((timeGap*shapes[indexValue].pics.length)+(timeGap+1000)).hide( "explode", {pieces: 9 }, 1000, function(){
                  $("#"+currentShape+"page img").hide();
                  $("#"+currentShape+"final").show("explode", {pieces: 9 },1000);
                  $("#"+currentShape+"next").delay(timeGap*2).show('slide', {direction: 'right'}, 500 );
            } );

    }

    function bookDesign(currentShape){
      // showing currentpage and setting slidedirection for images resp
      $("#"+currentShape+"final").show();
      position=["top","top","right","top","top","bottom"];

      // translating images
      timeGap=3000;
      setTimeout(function(){
      for (img_count=0; img_count<shapes[indexValue].pics.length;img_count++){
        slidedirection = position[img_count];
       // $("#"+shapes[indexValue].pics[img_count]).css(slidedirection, '-320vh' );
        $("#"+shapes[indexValue].pics[img_count]).show().css(slidedirection, '-320vh' );
      // $("#"+shapes[indexValue].pics[0]).delay(timeGap*(img_count+1)).fadeIn().animate({[slidedirection]:'0'},2500);
        var show_image = anime({
                     targets: "#"+shapes[indexValue].pics[img_count],
                     opacity: 1,
                     [slidedirection]: '0',
                     delay:timeGap*(img_count+1),
                     duration: 1000,
                     easing:"linear"
                   });
        $("#"+shapes[indexValue].pics[img_count]).delay(timeGap*(img_count+1)+timeGap).fadeOut(1000);
        $("#"+currentShape+"next").delay(timeGap*shapes[indexValue].pics.length+timeGap).show('slide', {direction: 'right'}, 500 );
      }
      },5000);
    }// bookDesign(currentShape)

    function fadeInRotate(currentShape){
      $("#"+currentShape+"page .row img").css({"position":"absolute"}).fadeIn(1000);
      targetPosition=["right","right","left","left"];
      translateX=[0,0,'5vw','5vw'];
      translateY=[0,0,'40vh','40vh'];
      timeGap=0;
      for(img_count=shapes[indexValue].pics.length;img_count>=-1;img_count--){

        timeGap+=2500;
        position=targetPosition[img_count];
        var show_image = anime({
                     targets: "#"+shapes[indexValue].pics[img_count],//dice
                     translateX: translateX[img_count],
                     translateY: translateY[img_count],
                     [position]:0,
                     rotate: '1turn',
                     delay:timeGap,
                     duration: 2000,
                     easing:"linear"
                   });
        }
        //since timeGap here is timeGap+=2000;after loop exec timeGap= 8000
          $("#"+currentShape+"page .row img").delay(timeGap).hide( "explode", {pieces: 9 }, 1000,function(){
            $("#"+currentShape+"page .final").css("border-radius",'0px').show("explode",{pieces:9},1000);
            $("#"+currentShape+"next").delay(2000).show('slide', {direction: 'right'}, 500);
          });
    }// fadeInRotate
    function blindFall(currentShape){
    	img_count=0;
    	timeGap=2500;
    	$('#'+currentShape+'page').addClass("blindFall");
    	for (img_count=0; img_count<shapes[indexValue].pics.length;img_count++){

    		$('#'+shapes[indexValue].pics[img_count]).delay(timeGap).show('blind',function(){
    			$(this).delay(3000).hide("drop");
    		});
    		timeGap+=4000
    	}
    	$('#'+currentShape+'final').css("border-radius",'0px').delay(timeGap).show("blind");
    	$("#"+currentShape+"next").delay(timeGap+4000).show('slide', {direction: 'right'}, 500);
    }

    function scaleFade(currentShape){
	    img_count=0;
	    timeGap=4000;
	    for (img_count=0; img_count<shapes[indexValue].pics.length;img_count++){
	    	$("#"+shapes[indexValue].pics[img_count]).delay(timeGap).show("scale",2000,function(){

	    		$(this).delay(2000).fadeOut(1000);

	    	});
	    	timeGap+=6000;
	    	}
	    $('#'+currentShape+'final').css("border-radius",'0px').delay(timeGap).show("blind");
    	$("#"+currentShape+"next").delay(timeGap+2000).show('slide', {direction: 'right'}, 500);
    }
    
    function blindUncover(){
	  findIndex(); 
	  $("#"+currentShape+"page").addClass("blindUncover");
	  timeGap=4000;
	  img_count=0;
	  slideInterval=setInterval(function(){
	    if(img_count==shapes[indexValue].pics.length-1){
		$("#"+currentShape+"final").show("blind",{direction:"right"},2000,function() {
		  $("#"+currentShape+"next").delay(2000).show("slide",{direction:"right"},500);
		  clearTimeout(slideInterval);
		 // alert("end");
		});

	    }else{	    
	      $("#"+shapes[indexValue].pics[img_count]).delay(1000).show("blind",{direction:"right"},2000,function(){		
		  img_count++;		
	      });
	    }
	  },timeGap);
	}
    function puffTransfer(currentShape){
       $('#'+currentShape+'page').addClass("puffTransfer");
       timeGap=2000;
       img_count=0;
       //$('.puffTransfer img').css("opacity",0).show();
       $('#crumpledpage img').each( function(index){
        //alert("current timeGap "+timeGap);
      var context=$(this);
      setTimeout(function(){
         context.show("puff",{percent:10},2000,function(){
            if(index < ($('#'+currentShape+'page img').length)-1){
               var cssSelector = anime({
                 targets:"#"+context.attr("id"),
                 translateX: "75%",
                 translateY:"50%",
                 delay:2000,
                 opacity:1,
                 duration:2000,
                 easing:"linear",
                 scale:0
               });
           }else{
             $("#"+currentShape+"next").delay(4000).show('slide', {direction: 'right'}, 500);
           }
         });

       },timeGap);
       timeGap=timeGap+6500;
       });
	}
	
	function scaleZoom(){
	  $("#"+currentShape+"page").addClass("fadeZoom");
	  $("#"+currentShape+"page img").fadeIn();  
	  $("#"+currentShape+"page img").addClass("zoomin");
	  findIndex();
		timeGap=4000;
		img_count=0;
	  zoomInterval=setInterval (function(){
		if(img_count>shapes[indexValue].pics.length-1){
		 $("#"+currentShape+"next").show("slide",{direction:"right"},500);
		  clearTimeout(zoomInterval);	
		 
		}else{
		  $("#"+shapes[indexValue].pics[img_count]).removeClass("zoomin");
		  $("#"+shapes[indexValue].pics[img_count]).addClass("zoom");
		  img_count++;
		}
		},timeGap);

	}
	function slideFade(currentShape){
	  findIndex(); 
	  $("#"+currentShape+"page").addClass("slideFade");
	  timeGap=3000;
	  directions=["down","right","left","right","up","left"];
	  dir_count=0;
	  img_count=0;
	  slideInterval=setInterval(function(){
	    if(img_count==shapes[indexValue].pics.length-1){
		$("#"+currentShape+"final").fadeIn(2000,function() {
		  clearTimeout(slideInterval);
		  $("#"+currentShape+"next").delay(1000).show("slide",{direction:"right"},500);
		});

	    }else{
	      timeGap=5000;
	      //alert(img_count);
	      $("#"+shapes[indexValue].pics[img_count]).stop().delay(500).show("blind",{direction:directions[dir_count]},2000,function(){
		$(this).stop().delay(2000).fadeOut(2000,function() {
		  img_count++;
		  dir_count++;
		});
	      });
	    }
	  },timeGap);
	}

function explodeShow() {
  $("#"+currentShape+"page").addClass("explodeShow");
  findIndex();
  timeGap=4000;
  img_count=0;
  explodeInterval=setInterval(function() {
    if(img_count==shapes[indexValue].pics.length){
      clearTimeout(explodeInterval);
      $("#"+currentShape+"next").delay(4000).show("slide",{direction:"right"},500);
    }else{
    $("#"+shapes[indexValue].pics[img_count]).delay(2000).show("explode",2000);
    img_count++;
    }
  },timeGap);
}

    /************************************************blue link end****************************************************/
    $(".activity").click(function() {
           var currentObject = $(this);
           currentShape = currentObject.attr("data-shape");
           currentActivity = currentObject.attr("id");
           resetActivity();
           setDialog();
           setBg();
           dataJson();
           findIndex();
    });// activity ends
    function resetActivity(){
      count = 0;
      right = 0;
      wrong = 0;
      correct = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
      correct.sort(randOrd);
      $("#choices").empty();
      $("#pallet").hide();
      $(".pages").hide();
      $("#activitylink").next().hide();
    }

    function setDialog(){
      $('#dialog').dialog({
          title: "GAME OVER!!!",
          closeOnEscape: false,
          autoOpen: false,
          resizable: false,
          draggable: false,
          width: 300,
          height: 220,
          modal: true
      });
    }
    function setBg(){
      $("#bg-container").css({
          "background": 'white',
          "background-size": "100% 100%"
      });

    }
    function dataJson(){
      data = $.getJSON("json/" + currentActivity + ".json", function() {
          quest = data.responseJSON.questions;
          quest.sort(randOrd);
          quest[count].images.sort(randOrd);
          $("#quest").trigger("play");
          renderQuestions();
      });
    }

    function renderQuestions() {
        $("#choices").empty();
        $("#activity").fadeIn(1000);
        ans = quest[count].ans;
        quest[count].images.sort(randOrd);
        $("#qnumber").html("<span class='badge badge-warning'>QNo: " + (count + 1) + "/" + quest.length + "</span>");
        $("#score").html("&nbsp; &nbsp; &nbsp;<span class='badge badge-success'> ശരി : " + right + "/" + quest.length + " </span> &nbsp; &nbsp; &nbsp;<span class='badge badge-danger'> തെറ്റായ ശ്രമങ്ങൾ : " + wrong  + "</span>");
        $("#questions").html(quest[count].que);
        quest[count].images.sort(randOrd);
        for (i = 0; i < quest[count].images.length; i++) {
            $("#choices").append('<div id="img' + (i + 1) + '"class="col-12 col-sm-6 col-md-6 col-lg-3 choice"> <button class="btn btn-secondary" data-value="' + quest[count].images[i] + '"><img src="images/' + currentActivity + '/' + quest[count].images[i] + '.png"/></button> </div');
            //$(".option").removeClass("btn-success btn-danger").addClass("btn-primary").attr("disabled", false);
        }

    }

    $(document).on("click", "#choices button", function() {
        $("#buttonclick").trigger("play");
        clicked = $(this).attr("data-value");

        //Validating answer
        if (clicked == ans) {

            $("#response").attr("src", "sounds/f_sounds/correct" + correct[count] + ".ogg").trigger('play');
            $("#correctclip").trigger("play");
            $(this).addClass("btn-success");
            $("#choices button").attr('disabled', true);
            right++;
            count++;

        } else {

            $(this).addClass("btn-danger").attr('disabled', true);
            wrong++;
            $("#wrongclip").trigger("play");
        }
        $("#score").html("&nbsp; &nbsp; &nbsp;<span class='badge badge-success'> ശരി : " + right + "/" +quest.length + " </span> &nbsp; &nbsp; &nbsp;<span class='badge badge-danger'> തെറ്റായ ശ്രമങ്ങൾ : " + wrong +  "</span>");

    });

    $("#response").on('ended', function() {
        if (count < quest.length ) {
            $("#activity").fadeOut(1000, function() {
            	$("html, body").animate({ scrollTop: 0 });
                renderQuestions();
            });
        } else {
            reset = 0;
            Accuracy = Math.floor(right / (right + wrong) * 100);
            $("#accuracy").html("<b>Accuracy: " + Accuracy + " %</b>");
            if(currentActivity=="lineactivity"){
              $("#next").removeClass("next").addClass("home").html("Home");
            }else{
              $("#next").removeClass("home").addClass("next").html("അടുത്തത്");
            }
            $("html, body").animate({ scrollTop: 0 },function(){
            	$("#gameover").trigger("play");
              $('#dialog').dialog('open');

            });
        }
    });

    $(".next").click(function() {
        menuclicked++;
        $("#activitylink").next().hide();
        $(".dropdown").removeClass("show");
 	$("#activitylink").addClass("disabled");
        nextIndex=indexValue+1;
        indexValue= nextIndex;
        $('.dialog').dialog('destroy');
        currentShape = shapes[nextIndex].shape;
        ColorValue= shapes[nextIndex].color;     
        $("#bg-container").hide('slide', {direction: 'left'}, 1000,function(){
            showPage(currentShape,ColorValue,designs[nextIndex]);
          $("#bg-container").show();
        });
    });

    $(".reset").click(function() {

        $('.dialog').dialog('destroy');
        resetActivity();
        setDialog();
        setBg();
        dataJson();

      /*  $(".nav li.dropdown").addClass('disabled');
        $(".nav li.dropdown").removeClass('open');
        $(".nav li.dropdown a").attr('data-toggle', '');*/
    });


    $(".nextpagebutton").click(function() {
   	 menuclicked=0;
   	 $("#activitylink").removeClass("disabled");
        currentActivity= currentShape+"activity";
        $("#bg-container").hide('slide', {direction: 'left'}, 1000,function(){
          setBg();
          resetActivity();
          setDialog();
          dataJson();
          $("#"+currentShape+"page").hide();
          $("#bg-container").show( "slide", {direction: "left"},1000);
        });
    });
    /****************************************************activiy end************************************************************/
});
