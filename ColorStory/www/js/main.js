$(document).ready(function() {
    reset = 0;
    currentcoloractivity = 0;
    //$("#bg").trigger("play");
    $("#audio1").trigger("play");
    var colors = anime({
        targets: '#colors .el',
        backgroundColor: [{
                value: '#FFF'
            }, // Or #FFFFFF
            {
                value: 'rgb(255, 0, 0)'
            },
            {
                value: 'hsl(100, 60%, 60%)'
            }
        ],
        easing: 'linear',
        direction: 'alternate',
        duration: 4000
    });

    $('.pages, .kingfisher, #pallet').hide();
    $('#homepage').show();

    function randOrd() {
        return (Math.round(Math.random()) - 0.5);
    }

    /*********************************************************************************************************************/
    $("#homelink").click(function() {
        location.reload();

    }); // homelink
    $(".menucolors").click(function() {
      // alert($(this).hasClass( "bluecolor" ));
       $("#bg-container").css('background', 'white');
       
    }); // homelink

    /************************************************home end*************************************************************************/

    $('.bluelink').on('click', bluelinkclicked);

    function bluelinkclicked() {

        $('audio').each(function() {
            this.pause(); // Stop playing
            this.currentTime = 0; // Reset time
        });
        $("#audio2").trigger('play');
        $('.pages, .kingfisher,.currentcoloractivity').hide();
        $("#bg-container").css({
            "background": 'url("images/blue/sky.jpg") no-repeat center center fixed',
            "background-size": "100% 100%"
        });
        $('.bluecolors').css({
            'position': 'absolute',
            'left': '-85%',
            'transform': 'none'
        });
        $('#bluepage').fadeIn(1000, function() {
            var bluecolors = anime({
                targets: '.bluecolors',
                left: '0',
                easing: 'easeInOutQuad',
                duration: 3000

            });
            var promise = bluecolors.finished.then(circle_animate);

        }); //blue page       
        function circle_animate() {
            var circle = anime({
                targets: '.circle',
                opacity: 0,
                delay: 3000,
                easing: 'easeInOutQuad',
                duration: 1000

            }); // circle
            var promise = circle.finished.then(bluecircle);

            function bluecircle() {
                $("#audio3").trigger('play');
                var blue = anime({
                    targets: '#blue',
                    opacity: 1,
                    easing: 'easeInOutQuad',
                    duration: 1000
                });
                var promise = blue.finished.then(lightbluecircle);

            } // bluecircle
            function lightbluecircle() {
                var lightblue = anime({
                    targets: '#lightblue',
                    opacity: 1,
                    easing: 'easeInOutQuad',
                    duration: 1500
                });
                var promise = lightblue.finished.then(darkbluecircle);
            } // lightbluecircle
            function darkbluecircle() {
                var darkblue = anime({
                    targets: '#darkblue',
                    opacity: 1,
                    easing: 'easeInOutQuad',
                    duration: 2000
                });

                var promise = darkblue.finished.then(seabluecircle);
            } // darkbluecircle
            function seabluecircle() {
                var bluepearl = anime({
                    targets: '#bluepearl',
                    opacity: 1,
                    easing: 'easeInOutQuad',
                    duration: 1000

                });
                var promise = bluepearl.finished.then(colordetails);
            } //seabluecircle


            function colordetails() {
                $('.bluecolors').delay(1000).fadeOut(1000, function() {
                    $("#audio4").trigger('play');
                    $("#pointer").fadeIn(1000);
                    var body = anime({
                        targets: '#bg-container',
                        scale: 2,
                        easing: 'linear'
                    });
                    var promise = body.finished.then(originalimage);
                }); // blucolors fadeout

                function originalimage() {
                
                    var original = anime({
                        targets: '#bg-container',
                        delay: 1000,
                        scale: 1,
                        duration: 1000,
                        easing: 'linear'
                    });     
                    $("#pointer").delay(1000).fadeOut(1000);       
                    var promise = original.finished.then(greengrass);
                } //originalimage

                function greengrass() {
                    
                    $("#audiograss").trigger('play');  
                    $("#pointer").css({right:'-41vw',top:0}).fadeIn(1000); 
                    $("#pointer").delay(4000).fadeOut(1000);                
                    $("#bg-container").css({
                        "background": 'url("images/blue/greengrass.jpg") no-repeat center center fixed',
                        "background-size": "100% 100%"
                    }).delay(6000).fadeOut(0, function() {
                        
                        $("#bg-container").css({
                            "background": 'url("images/blue/sky2.jpg") no-repeat center center fixed',
                            "background-size": "100% 100%"
                        }).show();
                        $('.bluelink').on('click', bluelinkclicked);
                        $('.kingfisher').delay(1000).fadeIn(1000, function() {                       
                            $("#audio6").trigger('play');
                            $("#pointer").css({right:'-26vh',bottom:'-45vh'}).delay(1500).fadeIn(1000);                            
                          

                        }); //bg-container fadeIn

                    }); // bg-container fadeOut

                  $("#bg-container").delay(8000).fadeOut(0, function() {
                        $('.kingfisher').hide();
                        $("#pointer").hide();
                        $("#audio7").trigger('play');
                        $("#pointer").css({bottom:'-85vh',top:"20vh", right:"-22vh"}).delay(2000).fadeIn(1000);      
                        $("#bg-container").css({
                            "background": 'url("images/blue/sky3.jpg") no-repeat center center fixed',
                            "background-size": "100% 100%"
                        }).fadeIn(0);
                        var body = anime({
                            targets: '#bg-container',
                            delay: 2000,
                            scale: 2,
                            duration: 1000,
                            easing: 'linear'
                        });
                        var promise = body.finished.then(bodydefault);


                    });//bg-container fadeOut	 

                } // greengrass
                function bodydefault() {
                    var bodyoriginal = anime({
                        targets: '#bg-container',
                        delay: 2000,
                        scale: 1,
                        duration: 1000,
                        easing: 'linear'
                    });
		    $("#pointer").delay(4000).fadeOut(1000);
                    var promise = bodyoriginal.finished.then(sunset);
                } //bodydefault
                function sunset() {

                    $("#bg-container").delay(2000).fadeOut(0, function() {
                        $("#bg-container").show().css({
                            "background": 'url("images/blue/sunset.jpg") no-repeat center center fixed',
                            "background-size": "100% 100%"
                        });
                        $("#audio8").trigger('play');
                         $("#pointer").delay(1000).css({right:'-35vw',top:'0',bottom:'0'}).fadeIn(1000);
                          $("#pointer").delay(5000).fadeOut(1000);
			$("#bluenext").delay(6000).fadeIn(1000);
                    })

                } //sunset			 


            } //colordetails

        } // circleanimate end

    } //bluelinkclicked

    /************************************************blue link end********************************************************************/

    $('.orangelink').on('click', orangelinkclicked);

    function orangelinkclicked() {

        $('audio').each(function() {
            this.pause(); // Stop playing
            this.currentTime = 0; // Reset time
        });
        $("#audio9").trigger('play');
        $('.pages, .orangeobj,.currentcoloractivity').hide();
        $("#orangepage,#pallet, #pallet img").show();
        //$("#bg-container").css('background', 'rgb(243, 147, 55)');
        $("#pallet img").attr('src', "images/orange/orangepallet.png");
        $("#carrot").delay(4000).fadeIn(1000, function() {
            $("#audio10").trigger('play');
            var carrot = anime({
                targets: '#carrot',
                delay: 1000,
                scale: '0.4',
                translateX: '-60%',
                translateY: '-50%',
                duration: 1000,
                easing: 'linear'
            });
            $("#orangefruit").delay(2000).fadeIn(1000, function() {

                var orangefruit = anime({
                    targets: '#orangefruit',
                    delay: 1000,
                    scale: '0.5',
                    translateX: '60%',
                    translateY: '-60%',
                    duration: 1000,
                    easing: 'linear'
                });

                $("#yam").delay(2000).fadeIn(1000, function() {
                    var yam = anime({
                        targets: '#yam',
                        scale: '0.4',
                        translateX: '100%',
                        translateY: '300%',
                        duration: 1000,
                        easing: 'linear'
                    });
                    $("#orangeflower").delay(1000).fadeIn(1000, function() {
                        var orangeflower = anime({
                            targets: '#orangeflower',
                            scale: '0.3',
                            translateX: '-12%',
                            translateY: '-37%',
                            duration: 1000,
                            easing: 'linear'
                        });
                        $("#pumpkin").delay(1000).fadeIn(1000, function() {
                            var pumpkin = anime({
                                targets: '#pumpkin',
                                scale: '0.35',
                                translateX: '-95%',
                                translateY: '40%',
                                duration: 1000,
                                easing: 'linear'
                            });
                            var promise = pumpkin.finished.then(shinyorange);
                        }); //pappaya 
                    }); // orangeflower	  
                }); // yam 
            }); //orange

        }); //carrot
        function shinyorange() {

            $('.notshiny').delay(7000).fadeOut(1000, function() {
                $("#audio11").trigger('play');
                var orangeshiny = anime({
                    targets: '#orangefruit',
                    scale: '1',
                    translateX: '6%',
                    translateY: '150%',
                    duration: 1000,
                    easing: 'linear'
                });
                $("#orangenext").delay(6000).fadeIn(1000);
                
            }); //notshiny

        } // shinyorange
    } // orangelinkclicked

    /****************************************************orangelink end************************************************************/

    $(".menucolors").click(function() {
        $(".nav li.dropdown").addClass('disabled');
        $(".nav li.dropdown").removeClass('open');
        $(".nav li.dropdown a").attr('data-toggle', '');
    });

    $(".activity").click(function() {
        $("#pallet img").hide();
        count = 0;
        right = 0;
        wrong = 0;
        correct = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
       
        $(".choices").empty();
        $(".pages").hide();
        //newcolor=$(this).attr("id");
        if (currentcoloractivity == 0 && reset==0) {
            color = $(this).attr("id");
            nextcolor = $(this).parent().next().children().attr("id");
            currentcolor = color + "activity";
        }
        if (reset == 0) {
            color = $(this).attr("id");
            nextcolor = $(this).parent().next().children().attr("id");
            currentcolor = color + "activity";
        }
         //alert("dialog "+color+" init");
        $('.dialog').dialog({
            title: "GAME OVER!!!",
            autoOpen: false,
            resizable: false,           
            draggable: false,

            width: "300",
            height: 200,
            modal: true
        });

        correct.sort(randOrd);
        $("#" + currentcolor).show();
        $("#bg-container").css({
            "background": 'white',
            "background-size": "100% 100%"
        });

        data = $.getJSON("json/" + currentcolor + ".json", function() {
            quest = data.responseJSON.questions;
            quest.sort(randOrd);
            $("#quest").trigger("play");
            renderquestions();
        });
    });

    function renderquestions() {
        $(".choices").empty();
        $("#" + currentcolor).fadeIn(1000);
        ans = quest[count].ans;
        $(".qnumber").html("<span class='label label-warning'>QNo: " + (count + 1) + "/" + quest.length + "</span>");
        $(".score").html("&nbsp; &nbsp; &nbsp;<span class='label label-success'> ശരി : " + right + "/" + quest.length + " </span> &nbsp; &nbsp; &nbsp;<span class='label label-danger'> തെറ്റായ ശ്രമങ്ങൾ  : " + wrong +  "</span>");
        $(".questions").html(quest[count].que);$(".currentcoloractivity").delay(4000).fadeIn(1000);
        quest[count].images.sort(randOrd);
        for (i = 0; i < quest[count].images.length; i++) {
            $(".choices").append('<button id="img' + (i + 1) + '"class="choice btn btn-default" data-value="' + quest[count].images[i] + '"> <img src="images/' + currentcolor + '/' + quest[count].images[i] + '.png"/> </button');
            //$(".option").removeClass("btn-success btn-danger").addClass("btn-primary").attr("disabled", false);
        }

    }

    $(document).on("click", ".choices button", function() {
        $("#buttonclick").trigger("play");
        clicked = $(this).attr("data-value");
        if (clicked == ans) {

            $("#response").attr("src", "sounds/correct/correct" + correct[count] + ".ogg").trigger('play');
            $("#correctclip").trigger("play");
            $(this).addClass("btn-success");
            $(".choices button").attr('disabled', true);
            right++;
            count++;

        } else {

            $(this).addClass("btn-danger").attr('disabled', true);
            wrong++;
            $("#wrongclip").trigger("play");
        }
        $(".score").html("&nbsp; &nbsp; &nbsp;<span class='label label-success'> ശരി : " + right + "/" + quest.length + " </span> &nbsp; &nbsp; &nbsp;<span class='label label-danger'> തെറ്റായ ശ്രമങ്ങൾ  : " + wrong + "</span>");

    });

    $("#response").on('ended', function() {
        if (count < quest.length) {
            $("#" + currentcolor).fadeOut(1000, function() {
            	$("html, body").animate({ scrollTop: 0 });
                renderquestions();
            });
        } else {
            reset = 0;
            Accuracy = Math.floor(right / (right + wrong) * 100);
            $(".accuracy").html("Accuracy :" + Accuracy + " %");
            $("html, body").animate({ scrollTop: 0 },function(){
            	$("#gameover").trigger("play");
             
            $('#dialog'+color).dialog('open');
            });
             
           
        }

    });

    $(".next").click(function() {
        $('.dialog').dialog('destroy');
        $("." + nextcolor + "link").trigger("click");

    });
    $(".reset").click(function() {
        reset = 1;
        $('.dialog').dialog('destroy');

        $(".activity").trigger("click");
        $(".nav li.dropdown").addClass('disabled');
        $(".nav li.dropdown").removeClass('open');
        $(".nav li.dropdown a").attr('data-toggle', '');
    });
    $("#home").click(function() {
    	 location.reload();
    
    });
     $(".currentcoloractivity").click(function() {
         $("#buttonclick").trigger("play");
        $(".currentcoloractivity").hide();
        currentcoloractivity = 1;
        reset = 1;
        color = $(this).attr("data-color");
        nextcolor = $(this).attr("data-nextcolor");
        currentcolor = color + "activity";
        //alert(color);
        $(".activity").trigger("click");

    });
    

    /****************************************************activiy end************************************************************/
    $('.redlink').on('click', redlinkclicked);

    function redlinkclicked() {
        $('audio').trigger("pause");
        $('.pages, .currentcoloractivity, .orangeobj,.redobjs img').hide();
        $("#redpage,#pallet, #pallet img").show();
        //$("#bg-container").css('background', '#f81616');
        $("#pallet img").attr('src', "images/red/redpallet.png");
        $('#audio12').trigger("play");
        $("#butterfly, #img1").delay(6000).fadeIn(1000, function() {
            $("#audio13").trigger('play');
            var butterfly = anime({
                targets: '#butterfly',
                opacity: 1,
                duration: 1000,
                easing: 'linear'
            });
            $("#balloon, #img2").delay(2000).fadeIn(1000, function() {

                var balloon = anime({
                    targets: '#balloon',
                    delay: 1000,
                    opacity: 1,

                    duration: 1000,
                    easing: 'linear'
                });
                $("#redrose, #img3").delay(2000).fadeIn(1000, function() {

                    var redrose = anime({
                        targets: '#redrose',
                        delay: 1000,
                        opacity: 1,

                        duration: 1000,
                        easing: 'linear'
                    });
                    $("#hibiscus, #img4").delay(2000).fadeIn(1000, function() {

                        var hibiscus = anime({
                            targets: '#hibiscus',
                            delay: 1000,
                            opacity: 1,

                            duration: 1000,
                            easing: 'linear'
                        });

                        $("#redchilly, #img5").delay(2000).fadeIn(1000, function() {

                            var redchilly = anime({
                                targets: '#redchilly',
                                delay: 1000,
                                opacity: 1,

                                duration: 1000,
                                easing: 'linear'
                            });
                            $("#tomato, #img6").delay(2000).fadeIn(1000, function() {

                                var tomato = anime({
                                    targets: '#tomato',
                                    delay: 1000,
                                    opacity: 1,

                                    duration: 1000,
                                    easing: 'linear'
                                });
                                var promise = tomato.finished.then(redfruits);


                            }); //tomato	
                        }); //chilly
                    }); //hibiscus
                }); // rose	 
            }); //balloon 	 
        }); // butterfly


    } // red

    function redfruits() {

        $("#redobj1").delay(2000).fadeOut(1000, function() {
            $(this).remove();
            //error: repeating audio..some looping error
            $("#audio14").trigger('play');
            $("#apple, #img7").delay(5000).slideDown(1000, function() {

                var apple = anime({
                    targets: '#apple',
                    opacity: 1,
                    delay: 1000,
                    duration: 1000,
                    easing: 'linear'
                });

                $("#cherry, #img8").delay(3000).slideDown(1000, function() {

                    var cherry = anime({
                        targets: '#cherry',
                        opacity: 1,
                        delay: 1000,
                        duration: 1000,
                        easing: 'linear'
                    });

                    $("#strawberry, #img9").delay(3000).slideDown(1000, function() {

                        var strawberry = anime({
                            targets: '#strawberry',
                            opacity: 1,
                            delay: 1000,
                            duration: 1000,
                            easing: 'linear'
                        });
                        $("#pomegranate, #img10").delay(3000).slideDown(1000, function() {

                            var pomegranate = anime({
                                targets: '#pomegranate',
                                opacity: 1,
                                delay: 1000,
                                duration: 1000,
                                easing: 'linear'
                            });

                            var promise = pomegranate.finished.then(redtogreen);
                        }); //pomegranate
                    }); //strawberry					
                }); //cherry	
            }); //apple
        }); //redobj1fadeout
    } // redfruits()
    function redtogreen() {
        $("#redobj2").delay(2000).fadeOut(1000, function() {
            $(this).remove();
            $('#audio15').trigger("play");
            $("#apple, #img11").delay(4000).slideDown(1000, function() {

                var apple = anime({
                    targets: '#apple',
                    opacity: 1,
                    delay: 1000,
                    duration: 1000,
                    easing: 'linear'
                });
                $("#royalapple, #img12").delay(2000).slideDown(1000, function() {

                    var royalapple = anime({
                        targets: '#royalapple',
                        opacity: 1,
                        delay: 1000,
                        duration: 1000,
                        easing: 'linear'
                    });

                    $("#goldenapple, #img13").delay(2000).slideDown(1000, function() {

                        var goldenapple = anime({
                            targets: '#goldenapple',
                            opacity: 1,
                            delay: 1000, 
                            duration: 1000,
                            easing: 'linear'
                        });



                        $("#yellowapple, #img14").delay(2000).slideDown(1000, function() {

                            var yellowapple = anime({
                                targets: '#yellowapple',
                                opacity: 1,
                                delay: 1000,
                                duration: 1000,
                                easing: 'linear'
                            });
                            $("#yellowgreen, #img15").delay(2000).slideDown(1000, function() {

                                var goldapple = anime({
                                    targets: '#yellowgreenapple',
                                    opacity: 1,
                                    delay: 1000,
                                    duration: 1000,
                                    easing: 'linear'
                                });
                                $("#greenapple, #img16").delay(2000).slideDown(1000, function() {

                                    var greenapple = anime({
                                        targets: '#greenapple',
                                        opacity: 1,
                                        delay: 1000,
                                        duration: 1000,
                                        easing: 'linear'
                                    });
                                    $("#rednext").delay(4000).fadeIn(1000);
                                   

                                }); //green apple
                            }); //yellowapple 
                        }) //goldapple
                    }); //golden apple
                }); //royalapple	
            }); //apple
        }); //redobj2 fadeout

    } //redtogreen	

    /****************************************************green/* page************************************************************/
    $('.greenlink').on('click', greenlinkclicked);

    function greenlinkclicked() {
        $('audio').trigger("pause");
        $('#audio16').trigger("play"); 
        $('.pages,.currentcoloractivity, .greenobj img').hide();
        $("#greenpage,#pallet, #pallet img").show();
       // $("#bg-container").css('background', '#2bbc2b');
        $("#pallet img").attr('src', "images/green/greenpallet.png");
        $("#grass, #img17").delay(3000).show("slide", {direction: "down"},3000,function(){
              $("#tree, #img18").delay(3500).show("slide", {direction: "down"},3000,function(){
              	   $("#chameleon, #img19").delay(4000).show("slide", {direction: "left"},2000,function(){
              	       $("#frog, #img20").delay(4000).show("slide", {direction: "down"},2000,function(){
              	           $("#mantis, #img21").delay(2000).show("slide", {direction: "up"},2000,function(){
              	                greenobj2();
              	           });//mantis
              	       });// frog
              	   });//chameleon
              });//tree
       });//greengrass
      }
      
        
       
    function greenobj2(){
     $("#greenobj1").delay(6000).fadeOut(1000, function() {
             $(this).remove();			 
       	     $("#greenobj2 div, #greenobj2 img").delay(2000).fadeIn(1000, function() {
	     		$(this).stop(true).fadeIn();
	                  
	                  // alert("fadein");
	                 $('#audio17').trigger('play');     
	                 var time_gap =0;     
		         
	      	          $('#greenobj2 div').each(function(index) {
	      			         console.log(index+":"+typeof(index));
	      			          var context = $(this);
	      			          time_gap = time_gap+5500;
	      			          setTimeout(function(){
	      			          context.addClass('zoom');
	      			           setTimeout(function(){	      			              
	      			               context.addClass('zoomout');
	      			               if(index==5){
	      			                     $('#greenobj2').delay(2000).fadeOut(1000,function(){
	      			                          $(this).remove();
	      			                 	   $("#greenobj3 div, #greenobj3 img").fadeIn(1000,function(){
	      			                 	        	
	      			                 	      $('#audio18').trigger('play');
	      			                 	      
	      			                 	      $("#greennext").delay(20000).fadeIn(1000, function(){
	      			                 	       
	      			                 	      });
	      			                 	      	     			                 	                                                  });//fadeOut green obj3
	      			                 });//if ends
	      			               }  
	      			              },3000);//setimeoutt
	      			          },time_gap);//settimeout
	       	 	        });//each
	      });//fadeIn greeobj2
    	 });//greenobj1
  }
/**************************************************brown page************************************************************/
     $('.brownlink').on('click', brownlinkclicked);

     function brownlinkclicked() {
        
         $('audio').trigger("pause");
         $('#audio19').trigger("play"); 
         $('.pages,.currentcoloractivity,.brownobj').hide();
         $("#brownpage,#pallet, #pallet img").show();
         //$("#bg-container").css('background', '#834f14');
         $("#pallet img").attr('src', "images/brown/brownpallet.png");
          var time_gap=0;
         $('#brownobj1 div').each(function(index) {
         var context=$(this);
         time_gap = time_gap+4000;
          setTimeout(function(){
         	context.fadeIn(1000,function(){  
         	   if(index==8){
         	     $("#brownnext").delay(1000).fadeIn(1000);
         	     } else{     	
         	    context.delay(2000).fadeOut(1000);
         	     }
         	   
         	    
         	});
         	},time_gap);
         });
        
	
    }
    /**************************************************violet page************************************************************/
     $('.violetlink').on('click', violetlinkclicked);
     function violetlinkclicked() {
        
         $('audio').trigger("pause");
         $('#audio20').trigger("play"); 
         $('.pages,.currentcoloractivity,.violetobj').hide();
         $("#violetpage,#pallet, #pallet img").show();
         //$("#bg-container").css('background', '#c15be7');
         $("#pallet img").attr('src', "images/violet/violetpallet.png");
         
          var time_gap=0;
         $('#violetobj div').each(function(index) {
         var context=$(this);
         time_gap = time_gap+4000;
          setTimeout(function(){
         	context.fadeIn(1000,function(){  
         	
         	   if(index==5){
         	    $("#violetnext").delay(1000).fadeIn(1000);
         	     } else{     	
         	    context.delay(2000).fadeOut(1000);
         	     }
         	   
         	    
         	});
         	},time_gap);
         });
        
    }
       /**************************************************yellow page************************************************************/
     $('.yellowlink').on('click', yellowlinkclicked);
     function yellowlinkclicked() {
        
         $('audio').trigger("pause");
         $('#audio21').trigger("play"); 
         $('.pages,.currentcoloractivity,.yellowobj').hide();
         $("#yellowpage,#pallet, #pallet img").show();
         //$("#bg-container").css('background', '#ecec2f');
         $("#pallet img").attr('src', "images/yellow/yellowpallet.png");
         
           var time_gap=0;
         $('#yellowobj div').each(function(index) {
         var context=$(this);
         time_gap = time_gap+4000;
          setTimeout(function(){
         	context.fadeIn(1000,function(){  
         	   if(index==7){
         	     $("#yellownext").delay(1000).fadeIn(1000);
         	     } else{     	
         	    context.delay(2000).fadeOut(1000);
         	     }        	            	    
         	});
         	},time_gap);
         });
    }
/************************************************* page************************************************************/
   
   /*************************************************************************************************************/ 
    
}); // ready function/




/*   $('.pages, .kingfisher').hide();

	   $('audio').each(function(){
                  this.pause(); // Stop playing
                  this.currentTime = 0; // Reset time
           });
	   //$("#audio1").trigger("play");
	   $("body").css({"background":'url("images/color.jpg") no-repeat center center fixed', "background-size":"100% 100%"});
	   
	   $('#homepage').fadeIn(1000,function(){
	   	var colors = anime({
		  targets: '#colors .el',
		  backgroundColor: [
		    {value: '#FFF'}, // Or #FFFFFF
		    {value: 'rgb(255, 0, 0)'},
		    {value: 'hsl(100, 60%, 60%)'}
		  ],
		  easing: 'linear',
		  direction: 'alternate',
		  duration: 4000
	        });
	        var bg = anime({
				  targets: '#bg-container',
				  delay:1000,
				  background:'url("images/color.jpg") no-repeat center center fixed',			    
				  duration: 1000,				   
				  easing: 'linear'			  
		           });
	   
	   });*/
