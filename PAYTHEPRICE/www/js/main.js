$(document).ready(function() {
        count= 0;
        right=0;
        wrong=0;
        total=0;
        amount=0;
        lng  = "mal";
        correct=[1,2,3,4,5,6,7,8,9,10];
        $(".game, #home, #score, .time, #questionscreen, #volume_icon, #payment").hide();
         $("#quesarea center").html("പണം ചിലവാക്കുക ");
                $("#option1 b").html("പണം തിരിച്ചറിയുക");
                $("#level1").html("ലെവൽ - 1");
                $("#option2 b").html("പണം എണ്ണുക ");
                $("#level2").html("ലെവൽ - 2");
                $("#option3 b").html("പണം താരതമ്യപെടുത്തുക ");
                $("#level3").html("ലെവൽ - 3");
                $("#right_span").html("ശരി ");
                $("#wrong_span").html("തെറ്റ് ");
                $("#qno").html("ചോദ്യം ");
                $("#button center").html('ഷോപ്പിംഗ് ലിസ്റ്റ് അനുസരിച്ച് സാധനങ്ങള്‍ വാങ്ങാന്‍ പോകുന്നതാണ് ഈ പ്രവര്‍ത്തനം.അതിനായി ഒരു <b>ലെവല്‍ </b>തിരഞ്ഞെടുക്കുക.');
        function reset(){
        
                count= 0;
                right=0;
                wrong=0;
                total=0;
                amount=0;
                $(".game, #home").hide();
                $("#listing_page,#copyright,#girl").show();
                $(".game, #home, #score, .time, #questionscreen, #volume_icon").hide();
                $(".time").TimeCircles().destroy();
                $("#correct").html(right);
                $("#wrong").html(wrong);
                $(".list").empty();
                $("audio").trigger("pause");
                $(".container").css("background","floralwhite");
                if (lng=="mal"){
                    $("#quesarea center").html("പണം ചിലവാക്കുക ");
                }else{
                     $("#quesarea center").html("PAY THE PRICE ");
                }
        }
        
        $(".lan").click(function(){
        
            $("#buttonclick").trigger("play");
            lng = $(this).attr("id");
            $(".lan").addClass("btn-danger").css("opacity","0.5");
    	    $(this).removeClass("btn-danger").addClass("btn-success").css("opacity","1");
            if (lng=="mal"){
            
                $("#quesarea center").html("പണം ചിലവാക്കുക ");
                $("#option1 b").html("പണം തിരിച്ചറിയുക");
                $("#level1").html("ലെവൽ - 1");
                $("#option2 b").html("പണം എണ്ണുക ");
                $("#level2").html("ലെവൽ - 2");
                $("#option3 b").html("പണം താരതമ്യപെടുത്തുക ");
                $("#level3").html("ലെവൽ - 3");
                $("#right_span").html("ശരി ");
                $("#wrong_span").html("തെറ്റ് ");
                $("#qno").html("ചോദ്യം ");
                $("#button center").html('ഷോപ്പിംഗ് ലിസ്റ്റ് അനുസരിച്ച് സാധനങ്ങള്‍ വാങ്ങാന്‍ പോകുന്നതാണ് ഈ പ്രവര്‍ത്തനം.അതിനായി ഒരു <b>ലെവല്‍ </b>തിരഞ്ഞെടുക്കുക.');
              
                
            }else{
            
                $("#quesarea center").html("PAY THE PRICE ");
                $("#option1 b").html("IDENTYFYING MONEY");
                $("#level1").html("LEVEL - 1");
                $("#option2 b").html("COUNTING MONEY");
                $("#level2").html("LEVEL- 2");
                $("#option3 b").html("COMPARING MONEY");
                $("#level3").html("LEVEL - 3");
                $("#right_span").html("Right");
                $("#wrong_span").html("Wrong");
                $("#qno").html("Question");
                $("#button center").html('In this activity you are going to buy items on shopping lists.<br> First, choose a <b>"level"</b>.');
            
            }
        });
        
        function randOrd(){
				return (Math.round(Math.random())-0.5);
  		}
        
        $(".level").click(function(){
             total=0;
             amount=0;
             correct.sort(randOrd);
             $(".payrupee").remove();
             $(".rupee").removeClass("btn-danger").addClass("btn-default");
             $("#buttonclick").trigger("play");
             levelId = $(this).attr('id');
             order=$(this).data("order");
             $("#listing_page,#copyright, #total").hide();
             $("#container_"+levelId+", #home, #score, .time, #questionscreen, #volume_icon").fadeIn();
             res=$.getJSON("JSON/"+lng+"/question"+order+".json",function(){
	   					items=res.responseJSON.questions;
	   					items.sort(randOrd);
	   					for(i=0;i<(items.length/2);i++){
			   		        $("#list"+order).append("<li>"+items[i].item+"<span id='value"+i+"'class='value'></span></li>"); 	
			   			}
			   			$(".value").hide();
			   			question();
             });
             if (order==3){
              $("#compare").show();
              $("#value").hide();
                if(lng=="mal"){
                    $("#quesarea center").html("ഏറ്റവും വില കുറഞ്ഞത്‌  തിരഞ്ഞെടുക്കുക ");
                 }else{
                    $("#quesarea center").html(" CHOOSE THE CHEAPEST OPTION");
                 }
  
                 
             }else{
                if(lng=="mal"){
                    $("#quesarea center").html("ശരിയായ പണം തിരഞ്ഞെടുകുക ");
                 }else{
                    $("#quesarea center").html(" CHOOSE THE CORRECT MONEY");
                 }
               
             }
             
             $("#questaudio").attr("src", "sounds/"+lng+"/choose"+order+".ogg").trigger("play");		
             $(".time").TimeCircles({
                    time: {
                        Days: {
                            show: false
                        },
                        Hours: {
                            show: false
                        }
                    }
            }).start();
        
        });
        
          
        $("#volume_icon").click(function(){		
				$("#questaudio").attr("src", "sounds/"+lng+"/choose"+order+".ogg").trigger("play");		
		});
  		
        function question(){
                              
                        $("#item"+order).html("<img src='images/"+items[count].name+".png' width='100%'>");
			   			$("#price"+order).html(items[count].price+"₹").data("ans",items[count].price);
			   			$(".option").removeClass("btn-success btn-danger").attr("disabled",false).addClass("btn-default");
			   			 if (order=="1"){
                            $("#img1 img").attr("src","images/currencies/"+items[count].img1+".png");
                            $("#img1").data("value",items[count].img1);	
			   			    $("#img2 img").attr("src","images/currencies/"+items[count].img2+".png");
			   			    $("#img2").data("value",items[count].img2);
                        }else if(order=="2"){
                            $("#img3 img").attr("src","images/currencies/"+items[count].img3+".png");	
			   			    $("#img4 img").attr("src","images/currencies/"+items[count].img4+".png");
			   			    $("#img5 img").attr("src","images/currencies/"+items[count].img5+".png");	
			   			    $("#img6 img").attr("src","images/currencies/"+items[count].img6+".png");
			   			    $("#img7 img").attr("src","images/currencies/"+items[count].img7+".png");	
			   			    $("#img8 img").attr("src","images/currencies/"+items[count].img8+".png");
			   			    $("#left").data("value",(parseInt(items[count].img3)+parseInt(items[count].img4)+parseInt(items[count].img5)));
			   			    $("#right").data("value",(parseInt(items[count].img6)+parseInt(items[count].img7)+parseInt(items[count].img8)));
                        }else{
                            
                            $("#img9 img").attr("src","images/"+items[count].items[0]+".jpg");
                            $("#img9").data("value",items[count].price[0]);	
                            $("#img10 img").attr("src","images/"+items[count].items[1]+".jpg");
                            $("#img10").data("value",items[count].price[1]);	
                            $("#img11 img").attr("src","images/"+items[count].items[2]+".jpg");
                            $("#img11").data("value",items[count].price[2]);	
                            $("#rate1").html(items[count].price[0]+"₹");
                            $("#rate2").html(items[count].price[1]+"₹");
                            $("#rate3").html(items[count].price[2]+"₹");
                        }
			   			
			   			$("#list"+order+" li").each(function(i) {	     
                             if ( i === count ) {
                             $(this).css({"color":"green"});
                             }else{
                              $(this).css({"color":"black"});
                             }
                             
                        });
                        $("#questionscreen #quest_no").html((count+1)+" / "+(items.length/2));
        }
	    
        $("#home").click(function() {
        
             $("#buttonclick").trigger("play");  
             reset();
        });
        
        $(".option").click( function(){
        
            if (order=="3"){
            ans=Math.min(items[count].price[0],items[count].price[1],items[count].price[2]);
            }else{            
            ans=$("#price"+order).data("ans");
            }
            
            $("#value"+count).html(" - "+ans+"₹");
            $(this).removeClass("btn-default");
            clicked=$(this).data("value");
            if(clicked==ans){
                total=total+parseInt(ans);         
                $(this).addClass("btn-success");
                $(".option").attr("disabled",true);
                $("#correctclip").trigger("play"); 
                $("#correctaudio").attr("src", "sounds/correct" + correct[(count + 1)] + ".ogg").trigger("play");
                right++;
                $("#correct").html(right);
                count++;
            }else{
                $(this).addClass("btn-danger").attr("disabled",true);
                $("#wrongclip").trigger("play"); 
                wrong++;
                $("#wrong").html(wrong);
            }
           
        
        });
     
          $('#correctaudio').on('ended', function() {
			
				if(count<(items.length/2)){
	   					    question();
	   			}
	   			else{
	   			        $("#completed").trigger("play");
	       			    $(".time").TimeCircles().stop();
	       			    accuracy= Math.round((right/(right+wrong))*100);
	       			    //alert("LEVEL COMPLETED\n Accuracy-"+accuracy+"%");
	   			
	   			
	   			    if (order=="3"){
	   			        alert("LEVEL COMPLETED\n Accuracy-"+accuracy+"%\n Proceed to 'CHECKOUT'");
	   			        $("#total").html("TOTAL - "+total+"₹");
	   			        $("#payment,#copyright, .value,#total").show();
	   			        $("#list"+order+" li").css("color","black");
	   			        order=4;
	   			        $("#questaudio").attr("src", "sounds/"+lng+"/choose"+order+".ogg").trigger("play");
	   			        if (lng=="eng"){
	   			            $("#quesarea center").html("CHOOSE THE CORRECT MONEY TO PAY.");
	   			        }else{
	   			           $("#quesarea center").html( "ശരിയായ തുക കണ്ടെത്തുക ");
	   			        }
	   			        $("#home, #score, .time, #questionscreen, #girl, #compare").hide();
	   			    }else{
	       			    alert("LEVEL COMPLETED\n Accuracy-"+accuracy+"%");
	       			    reset();
	   			    }
	   			}	
	    });
	    $(".rupee").click( function(){
	        $("#correctclip").trigger("play");
	        $(this).toggleClass("btn-danger");
	        btn=$(this).attr("class");
	        rupee=$(this).data("value");
	        rep=$(this).data("rep");
	        if($(this).hasClass("btn-danger")){
	            $("#pay").append("<img class='payrupee' id ='rupee_"+rep+"' src='images/currencies/"+rep+".png' width='20%'>");
	              amount=amount+parseInt(rupee);
	        }else{
	         $("#rupee_"+rep).remove();
	          amount=amount-parseInt(rupee);
	        }
	    });
	    
	    $("#paynow").click( function(){
	    
	        if(amount==total){
	             $("#completed").trigger("play");
	            alert("LEVEL COMPLETED\n Correct Amount Paid");
	            reset();
	        }else if(amount>total){
	            $("#wrongclip").trigger("play");
	            alert("Amount exceeds Total Pay");
	        }else{
	            $("#wrongclip").trigger("play");
	            alert("Amount is not sufficient to pay");
	        }
	    });
        
        
});
