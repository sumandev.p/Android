$(document).ready(function() {
        count= 0;
        right=0;
        wrong=0;
        total=0;
        amount=0;
        lng  = "mal";
        correct=[1,2,3,4,5,6,7,8,9,10];
        correct_count=0;
        $(".game, #home, #score, .time, #questionscreen, #volume_icon, .feedback").hide();
          $("#quesarea center").html("സുരക്ഷ നിയമങ്ങൾ ");              
                $("#level1").html("തുടങ്ങുക");
                $("#right_span").html("ശരി ");
                $("#wrong_span").html("തെറ്റ് ");
                $("#qno").html("ചോദ്യം ");
                 $("#no").html("ഇല്ല");
                 $("#yes").html("അതെ");
        function reset(){
        
                count= 0;
                right=0;
                wrong=0;
                total=0;
                amount=0;
                correct_count=0;
                $(".game, #home").hide();
                $("#listing_page,#copyright").show();
                $("body").css("background",'url("images/safety2.jpg") no-repeat scroll center bottom / 100vw auto');
                $(".game, #home, #score, .time, #questionscreen, #volume_icon").hide();
                $(".time").TimeCircles().destroy();
                $("#right").html(right);
                $("#wrong").html(wrong);
                $("audio").trigger("pause");
                if (lng=="mal"){
                     $("#quesarea center").html("സുരക്ഷ നിയമങ്ങൾ");
                }else{
                    $("#quesarea center").html("SAFETY RULES");
                }
                 $(".feedback").hide();
                 $("#ynbutton").show();
        }
        
        $(".lan").click(function(){
        
            $("#buttonclick").trigger("play");
            lng = $(this).attr("id");
            $(".lan").addClass("btn-danger").css("opacity","0.5");
    	    $(this).removeClass("btn-danger").addClass("btn-success").css("opacity","1");
            if (lng=="mal"){
            
                $("#quesarea center").html("സുരക്ഷ നിയമങ്ങൾ ");              
                $("#level1").html("തുടങ്ങുക");
                $("#right_span").html("ശരി ");
                $("#wrong_span").html("തെറ്റ് ");
                $("#qno").html("ചോദ്യം ");
                 $("#no").html("ഇല്ല");
                 $("#yes").html("അതെ");
              
                
            }else{
            
                $("#quesarea center").html("SAFETY RULES");
                $("#level1").html("START");
                $("#right_span").html("Right");
                $("#wrong_span").html("Wrong");
                $("#qno").html("Question");
                 $("#no").html("No");
                 $("#yes").html("Yes");
            
            }
        });
        
        function randOrd(){
				return (Math.round(Math.random())-0.5);
  		}
        
        $(".level").click(function(){
            $("#buttonclick").trigger("play");
             levelId = $(this).attr('id');
             order=$(this).data("order");
             $("#listing_page,#copyright").hide();
             $("#container_"+levelId+", #home, #score, .time, #volume_icon,#questionscreen").fadeIn();
             $("body").css("background","darkgrey");
             $(".time").TimeCircles({time: {Days: {show: false},Hours: {show: false}}}).start();
             correct.sort(randOrd);
            
                	
                res=$.getJSON("JSON/"+lng+"/question1.json",function(){
	   					items=res.responseJSON.questions;
	   					//items.sort(randOrd);
	   					
	   					questions();
	   					
	   			    });          
        });
        
      
        $("#volume_icon").click(function(){		
				$("#questaudio").trigger("play");		
		});
  		
        function questions(){
                 $("#ynbutton").show();
                 $("#quest_no").html((count+1)+" / "+items.length);
                 $(".choice").attr("disabled",false).removeClass("btn-success btn-danger").addClass("btn-warning");
                 $("#quesarea center").hide().html(items[count].ques).data("value",items[count].ans).fadeIn(1000,function(){
                    $("#questaudio").attr("src", "sounds/"+lng+"/"+items[count].audio+".ogg").trigger("play");
                 });
                 $("#img1").hide().attr("src","images/"+items[count].image+".jpg").fadeIn(1000);
             /*   images=items[count].images;
	   		    images.sort(randOrd);
                $(".level1_img").attr("disabled",false).removeClass("btn-success btn-danger").addClass("btn-info");
               
              
                $("#questaudio").attr("src", "sounds/"+lng+"/"+items[count].audio+".ogg").trigger("play");
                $("#img1").attr("src","images/"+items[count].ques_image+".jpg");
                if (lng=="eng"){
		            $("#img2").attr("src","images/"+images[0]+".jpg").data("value",images[0]);
		            $("#img3").attr("src","images/"+images[1]+".jpg").data("value",images[1]);
		            $("#img4").attr("src","images/"+images[2]+".jpg").data("value",images[2]);
		            $("#job1").html(images[0]);
		            $("#job2").html(images[1]);
		            $("#job3").html(images[2]);
		        }else{
                
                	$("#img2").attr("src","images/"+images[0].img+".jpg").data("value",images[0].job);
		            $("#img3").attr("src","images/"+images[1].img+".jpg").data("value",images[1].job);
		            $("#img4").attr("src","images/"+images[2].img+".jpg").data("value",images[2].job);
		            $("#job1").html(images[0].job);
		            $("#job2").html(images[1].job);
		            $("#job3").html(images[2].job);
                }*/
        }
	    $(".choice").click( function(){
	        $("audio").trigger("pause");
	        ans=$("#quesarea center").data("value");
	        clicked=$(this).data("value");
	         $(".feedback").removeClass("alert-success alert-danger");
	        $("#ynbutton").fadeOut(function(){
	            if(clicked==ans){
                 $("audio").trigger("pause");
                 
                 $("#correctclip").trigger("play"); 
                  fbaudio=clicked+"audio";
                 $(".feedback span").html(items[count][clicked]);
                 $(".feedback").addClass("alert-success");          
                 if (count==10){
                     	correct.sort(randOrd);
                     	correct_count=0;
                     }
                 $("#correctaudio").attr("src", "sounds/"+lng+ "/"+items[count][fbaudio]+".ogg").trigger("play");    
                 correct_count++;
                 right++;
                 $("#right").html(right); 
                 
               /* $(this).removeClass("btn-warning").addClass("btn-success");
                cr_attr= $(this).attr("disabled");
                if(cr_attr !="disabled"){
                   
                     
                     if (count==10){
                     	correct.sort(randOrd);
                     	correct_count=0;
                     }
                    $("#correctaudio").attr("src", "sounds/correct" + correct[(correct_count)] + ".ogg").trigger("play");
                    correct_count++;
                    count++;
                    right++;
                    $("#right").html(right);
                }
                 $(".choice").attr("disabled",true);*/
               
            }else{
           		fbaudio=clicked+"audio";
           		 $("#correctaudio").attr("src", "sounds/"+lng+ "/"+items[count][fbaudio]+".ogg").trigger("play");    
                $("#wrongclip").trigger("play");
                $(".feedback span").html(items[count][clicked]);
                $(".feedback").addClass("alert-danger");
                wrong++;
                $("#wrong").html(wrong);
               /* wr_attr=$(this).attr("disabled");
                if(wr_attr !="disabled"){
                    $(this).addClass("btn-danger").attr("disabled",true);
                     
                    wrong++;
                    $("#wrong").html(wrong);
                }
                $(this).attr("disabled",true);*/
            }
            
            $(".feedback").show();
	        
	        });
	       
	    });
	    $(".dismiss").click(function() {
	    	$("audio").trigger("pause");
	        $(".feedback").fadeOut(function(){
	              count++;
	              if (count< items.length){
	                questions();
	              }
	              else{
	                
                            $("#completed").trigger("play");
	           			    $(".time").TimeCircles().stop();
	           			    accuracy= Math.round((right/(right+wrong))*100);
	           			    alert("LEVEL COMPLETED\n Accuracy-"+accuracy+"%");
	           			    reset();
	              
	              }
	        });
	      
	    
        });
        $("#home").click(function() {
        
             $("#buttonclick").trigger("play");  
             reset();
        });
               
       /* $('#correctaudio').on('ended', function() {
     
                if (count< items.length){
                    questions();
                }else{
                            $("#completed").trigger("play");
	           			    $(".time").TimeCircles().stop();
	           			    accuracy= Math.round((right/(right+wrong))*100);
	           			    alert("LEVEL COMPLETED\n Accuracy-"+accuracy+"%");
	           			    reset();
               }
	    });*/
	  
        
        
});
