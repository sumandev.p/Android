package org.literacyapp.imagepicker;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.ResourceBusyException;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;


import org.literacyapp.imagepicker.util.MediaPlayerHelper;
import org.literacyapp.imagepicker.util.TtsHelper;


import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ListenAndSelectActivity extends AppCompatActivity {

   // private List<Word> wordsWithMatchingAudioAndImage;

  //  private List<Word> wordsCorrectlySelected;
    private List<String> newList;
    private List<String> newCorrect;
    private int i = -1,j=-1;
    private MediaPlayer mp;


    private int[] newArray = new int[]{R.drawable.pig,R.drawable.dog,R.drawable.bear,R.drawable.lion,R.drawable.plant,R.drawable.jaguar};
    List<String> newArray1;

    private int[] newVoice= new int[]{R.raw.number_one,R.raw.number_four,R.raw.number_eight,R.raw.number_five,R.raw.number_nine,R.raw.number_seven};

    private ArrayList<Integer> sounds;

    private ProgressBar progressBar;

    private CardView alt1CardView;
    private ImageView alt1ImageView;

    private CardView alt2CardView;
    private ImageView alt2ImageView;

    private Drawable defaultBackgroundDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(getClass().getName(), "onCreate");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_listen_and_select);

       // wordsCorrectlySelected = new ArrayList<>();


        progressBar = (ProgressBar) findViewById(R.id.listenAndSelectProgressBar);

        alt1CardView = (CardView) findViewById(R.id.alt1CardView);
        alt1ImageView = (ImageView) findViewById(R.id.alt1ImageView);

        alt2CardView = (CardView) findViewById(R.id.alt2CardView);
        alt2ImageView = (ImageView) findViewById(R.id.alt2ImageView);




        newCorrect = new ArrayList<>();
        newList = new ArrayList<>();
        newList.add("പന്നി");
        newList.add("സിംഹം ");


        newArray1 = new ArrayList<>();
        newArray1.add("പന്നി");
        newArray1.add("നായ");
        newArray1.add("കരടി");
        newArray1.add("സിംഹം");
        newArray1.add("ചെടി ");
        newArray1.add("പുള്ളിപ്പുലി ");

        sounds = new ArrayList<>();
        sounds.add(R.raw.panni);
        sounds.add(R.raw.naya);
        sounds.add(R.raw.karadi);
        sounds.add(R.raw.simham);
        sounds.add(R.raw.chedi);
        sounds.add(R.raw.pullippuli);




        // Fetch 10 most frequent words with matching audio and image
      /*  wordsWithMatchingAudioAndImage = new ArrayList<>();
        List<Word> words = ContentProvider.getAllWords(SpellingConsistency.values());
        Log.i(getClass().getName(), "words.size(): " + words.size());*/
        /*for (Word word : words) {
            Audio matchingAudio = ContentProvider.getAudio(word.getText());
            Image matchingImage = ContentProvider.getImage(word.getText());
            // TODO: add audio as requirement
            if (*//*(matchingAudio != null) &&*//* (matchingImage != null)) {
                Log.i(getClass().getName(), "Adding \"" + word.getText() + "\"...");
                Log.i(getClass().getName(), "matchingImage.getDominantColor(): " + matchingImage.getDominantColor());
                wordsWithMatchingAudioAndImage.add(word);
            }
            if (wordsWithMatchingAudioAndImage.size() == 10) {
                break;
            }
        }*/
       // Log.i(getClass().getName(), "wordsWithMatchingAudioAndImage.size(): " + wordsWithMatchingAudioAndImage.size());

        defaultBackgroundDrawable = getWindow().getDecorView().getBackground();
    }

    @Override
    protected void onStart() {
        Log.i(getClass().getName(), "onStart");
        super.onStart();

        loadNextImage();
    }

    public static int getDominantColor(Bitmap bitmap) {
        Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, 1, 1, true);
        final int color = newBitmap.getPixel(0, 0);
        newBitmap.recycle();
        return color;
    }

    public void clear(){
        newList.clear();
        newList.add(newArray1.get(j));
        newList.add(newArray1.get(j+1));
    }


    private void loadNextImage() {

        if (j < newArray1.size() - 2)
        {
            Log.i(getClass().getName(), "loadNextImage");
            i++;
            j++;
            if (newList.size() == 0) {
                newList.add(newArray1.get(j));
                newList.add(newArray1.get(j + 1));
            }

            // Reset background color
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
//        getWindow().getDecorView().setBackground(defaultBackgroundDrawable);
            getWindow().getDecorView().setBackground(new ColorDrawable(Color.parseColor("#EEEEEE")));
            progressBar.setProgress(newCorrect.size() * 100 / newArray1.size());
            alt1CardView.setBackgroundColor(Color.parseColor("#FFFFFF"));
            alt1ImageView.setAlpha(255);
            alt2CardView.setBackgroundColor(Color.parseColor("#FFFFFF"));
            alt2ImageView.setAlpha(255);

            //  final Word alt1Word = wordsWithMatchingAudioAndImage.get(wordsCorrectlySelected.size());
            //Log.i(getClass().getName(), "alt1Word.getText(): " + alt1Word.getText());
            getSupportActionBar().setTitle(newArray1.get(i));


            Bitmap alt1ImageBitmap = BitmapFactory.decodeResource(getResources(), newArray[j]);

            //    final Image alt1Image = ContentProvider.getImage(alt1Word.getText());
            //  File alt1ImageFile = MultimediaHelper.getFile(alt1Image);
            //  Bitmap alt1ImageBitmap = BitmapFactory.decodeFile(alt1ImageFile.getAbsolutePath());
            String alt1ImageDominantColor = String.valueOf(getDominantColor(alt1ImageBitmap));
            final int alt1ColorIdentifier = parseRgbColor(alt1ImageDominantColor);

       /* List<Word> otherWords = new ArrayList<>(wordsWithMatchingAudioAndImage);
        otherWords.remove(alt1Word);
        final Word alt2Word = otherWords.get((int) (Math.random() * otherWords.size()));
        Log.i(getClass().getName(), "alt2Word.getText(): " + alt2Word.getText());*/

            //  Image alt2Image = ContentProvider.getImage(alt2Word.getText());
            // File alt2ImageFile = MultimediaHelper.getFile(alt2Image);
            // Bitmap alt2ImageBitmap = BitmapFactory.decodeFile(alt2ImageFile.getAbsolutePath());

            Bitmap alt2ImageBitmap = BitmapFactory.decodeResource(getResources(), newArray[j + 1]);

            String alt2ImageDominantColor = String.valueOf(getDominantColor(alt2ImageBitmap));
            final int alt2ColorIdentifier = parseRgbColor(alt2ImageDominantColor);

            // TODO: play instruction audio


            String first = (String.valueOf(newArray[0]));
            Log.d("first", first);

            playWord();

            Collections.shuffle(newList);
            boolean isAlt1Correct = (Math.random() > .5);
            if (isAlt1Correct) {
                alt1ImageView.setImageBitmap(alt1ImageBitmap);
                alt2ImageView.setImageBitmap(alt2ImageBitmap);

                alt1CardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i(getClass().getName(), "alt1CardView onClick");

                        MediaPlayerHelper.play(getApplicationContext(), R.raw.alternative_correct);

                        //wordsCorrectlySelected.add(alt1Word);
                        newCorrect.add(newArray1.get(i));
                        newList.clear();
                        progressBar.setProgress(newCorrect.size() * 100 / newArray1.size());

                        getWindow().getDecorView().setBackgroundColor(alt1ColorIdentifier);
                        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(alt1ColorIdentifier));

                        alt1CardView.setOnClickListener(null);

                        alt2CardView.setOnClickListener(null);
                        alt2CardView.setBackgroundColor(Color.parseColor("#00FFFFFF")); // 100% transparent
                        alt2ImageView.setAlpha(128); // 50% opaque

                        alt1ImageView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                loadNextImage();
                            }
                        }, 2000);


                    }
                });

                alt2CardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i(getClass().getName(), "alt2CardView onClick");

                        MediaPlayerHelper.play(getApplicationContext(), R.raw.alternative_incorrect);

                        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_shake);
                        alt2CardView.startAnimation(animation);
                    }
                });
            } else {
                alt1ImageView.setImageBitmap(alt2ImageBitmap);
                alt2ImageView.setImageBitmap(alt1ImageBitmap);

                alt2CardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Log.i(getClass().getName(), "alt2CardView onClick");

                        MediaPlayerHelper.play(getApplicationContext(), R.raw.alternative_correct);

                        //  wordsCorrectlySelected.add(alt2Word);

                        //  progressBar.setProgress(wordsCorrectlySelected.size() * 100 / wordsWithMatchingAudioAndImage.size());
                        newCorrect.add(newArray1.get(0));
                        newList.clear();
                        progressBar.setProgress(newCorrect.size() * 100 / newArray1.size());

                        getWindow().getDecorView().setBackgroundColor(alt1ColorIdentifier);
                        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(alt1ColorIdentifier));

                        alt1CardView.setOnClickListener(null);

                        alt2CardView.setOnClickListener(null);
                        alt1CardView.setBackgroundColor(Color.parseColor("#00FFFFFF")); // 100% transparent
                        alt1ImageView.setAlpha(128); // 50% opaque

                        alt1ImageView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                loadNextImage();
                            }
                        }, 2000);
                    }
                });

                alt1CardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i(getClass().getName(), "alt1CardView onClick");

                        MediaPlayerHelper.play(getApplicationContext(), R.raw.alternative_incorrect);

                        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_shake);
                        alt1CardView.startAnimation(animation);
                    }
                });
            }
       /* newList.clear();
        newList.add(newArray1.get(j));
        newList.add(newArray1.get(j+1));*/

        }
    }

    private int parseRgbColor(String input) {
        Pattern pattern = Pattern.compile("rgb *\\( *([0-9]+), *([0-9]+), *([0-9]+) *\\)");
        Matcher matcher = pattern.matcher(input);
        if (matcher.matches()) {
            int rgbRed = Integer.valueOf(matcher.group(1));
            int rgbGreen = Integer.valueOf(matcher.group(2));
            int rgbBlue = Integer.valueOf(matcher.group(3));
            int colorIdentifier = Color.rgb(rgbRed, rgbGreen, rgbBlue);
            return colorIdentifier;
        } else {
            return -1;
        }
    }

    private void playWord() {

        //TtsHelper.speak(getApplicationContext(), word);


        mp = MediaPlayer.create(getApplicationContext(), sounds.get(i));
        mp.start();

     /*   Log.i(getClass().getName(), "playWord");

        // Look up corresponding Audio recording
        Log.d(getClass().getName(), "Looking up \"" + word.getText() + "\"");
        Audio audio = ContentProvider.getAudio(word.getText());
        Log.i(getClass().getName(), "audio: " + audio);
        if (audio != null) {
            // Play audio
            File audioFile = MultimediaHelper.getFile(audio);
            Uri uri = Uri.parse(audioFile.getAbsolutePath());
            MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    Log.i(getClass().getName(), "onCompletion");
                    mediaPlayer.release();
                }
            });
            mediaPlayer.start();
        } else {
            // Audio recording not found. Fall-back to TTS.
            TtsHelper.speak(getApplicationContext(), word.getText());
        }*/
    }
}
