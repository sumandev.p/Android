$(document).ready(function(){
	$(".sounds").trigger('load');
	lan="mal";
	$("#seq strong").text("കണ്ടെത്തുക");
			$("#find strong").text("തിരിച്ചറിയുക");
			$("#level1").text("ലെവൽ -1");
			$("#level2").text("ലെവൽ -2");
			$("#header center b").text("പഞ്ചേന്ദ്രിയങ്ങൾ ");	
	right=0,wrong=0,page=0,ansnum=0;
	$(".levels,#homeicon,#score,.timer,#levelsound,#question").hide();
	senses=[{"engsense":"Eyes","malsense":"കണ്ണ്‍ "},{"engsense":"Ears","malsense":"ചെവി"},{"engsense":"Nose","malsense":"മൂക്ക്"},{"engsense":"Hand","malsense":"കൈ"},{"engsense":"Tongue","malsense":"നാവ്‌"}];
	lev2order=["0","1","2"];
	soundno=["1","2","3","4","5","6","7","8","9","10","1","2"];
	$(".lan").click(function(){
		lan=$(this).attr("id");
		$(".lan").removeClass("btn-warning btn-success").addClass("btn-warning").fadeTo("fast",0.80);
		$(this).removeClass("btn-warning").addClass("btn-success").fadeTo("fast",1);;
		$("#btnclk").trigger('play');
		if(lan=="eng"){
			$("#seq strong").text("FIND OUT");
			$("#find strong").text("IDENTIFY");
			$("#level1").text("LEVEL-1");
			$("#level2").text("LEVEL-2");
			$("#header center b").text("THE FIVE SENSES");
		}else{
			$("#seq strong").text("കണ്ടെത്തുക");
			$("#find strong").text("തിരിച്ചറിയുക");
			$("#level1").text("ലെവൽ -1");
			$("#level2").text("ലെവൽ -2");
			$("#header center b").text("പഞ്ചേന്ദ്രിയങ്ങൾ ");	
		}
    	});



	$(".levbtn").click(function(){
		
		$("#levelsound,.timer,#homeicon,#score,#question").show();
		$("#frontpage,#space").hide();
		$("#rightscore").text(right);
		$("#wrongscore").text(wrong);
		var order=$(this).data("order");
		$("#page"+order).show();
		$(".descvolimg").attr("src","images/audio.png");	
		if(lan=="eng"){
				
			$("#rightdiv").text("Right-");
			$("#wrongdiv").text("Wrong-");
			$("#quesdiv").text("Question-");
		}else{
			
			$("#rightdiv").text("ശരി -");
			$("#wrongdiv").text("തെറ്റ് -");
			$("#quesdiv").text("ചോദ്യം -");
		}
		if(order==1){
	
			if(lan=="eng")
			{
				
				$("#head center b").text("Find Out The Right Answer");
				
			}else{
				
				$("#head center b").text("ശരിയായ ഉത്തരം കണ്ടെത്തുക");
				
			}
			
			$("#levsound").attr("src","sounds/level1/"+lan+"/head.ogg").trigger('play');
			jdata1=$.getJSON('json/level1.json', function(){
			jlev1=jdata1.responseJSON.findout;
			jlev1.sort(function() {return 0.5 - Math.random()});
 			displevel1();
			
			});
			
		}else{

			if(lan=="eng")
			{
				
				$("#head center b").text("Identify The Right Answers");
				
			}else{
				
				$("#head center b").text("ശരിയായ ഉത്തരങ്ങൾ തിരഞ്ഞെടുക്കുക");
				
			}
			$("#levsound").attr("src","sounds/level2/"+lan+"/head.ogg").trigger('play');
			jdata2=$.getJSON('json/level2.json', function(){
			jlev2=jdata2.responseJSON.findout;
			jlev2.sort(function() {return 0.5 - Math.random()});
 			displev2(page);
			});


		}

	});

	$(".optdesc,.lev2optn").mouseover(function(){ $(".timer").TimeCircles({time: {
		Days: {
		show: false
		},
		Hours: {
		show: false
		}
	
	}}).start(); });

	function reset(){
		$("#frontpage,#space").show();
		$("#homeicon,.levels,#score,#levelsound,#question").hide();
		page=right=wrong=next=0;
		$(".timer").TimeCircles().destroy();
		if(lan=="eng"){
			$("#head center b").text("THE FIVE SENSES");
			
			}else{
			$("#head center b").text("പഞ്ചേന്ദ്രിയങ്ങൾ");
 			
			}
	}
	
	$("#homeicon").click(function(){
		$("#qustn").trigger("pause");
		$("#btnclk").trigger('play');
		reset();
		
	});

	function displevel1(){
		$(".descdiv").trigger('pause');
		$(".descdiv").prop("disabled", false);
		//$("#qustn").show();
		$(".descdiv").removeClass("btn-danger btn-success").addClass("btn-warning");
		
		qus=jlev1[page][lan+"question"];
		$("#quesscore").text((page+1)+"/"+jlev1.length);
		$("#qustdiv1").text(qus).css({"text-align":"center","font-weight":"bold","font-size":"3vh","padding-top":"2vh","color":"black"});
		ques=jlev1[page]["sound"]+".ogg";
		
		$("#qustn").attr("src","sounds/level1/"+lan+"/"+ques);  
		$("#qustn").trigger('load');
		
	
		senses.sort(function() {return 0.5 - Math.random()});
		for(i=1;i<=5;i++)
		{
			order=senses[i-1];
			ln=lan+"sense";
			 $("#optimg"+i).attr('src',"images/level1/"+senses[i-1].engsense+".jpg");
			$("#descvolimg"+i).data("name",senses[i-1].engsense);
			$("#descdiv"+i).text(senses[i-1][ln]).css({"text-align":"center","font-weight":"bold","font-size":"2vh"}); 
		}
		$(".optdesc").show();

	}

	$(".descdiv").click(function(){

		if($(this).text()==(jlev1[page][lan+"answer"]))
		{	
			$("#qustn").trigger("pause")
			$(this).removeClass("btn-warning").addClass("btn-success");
			$("#corct").trigger('play');
			ansr=jlev1[page]["sound"]+"ans.ogg";
			$("qustdiv1").trigger("pause");
			$("#lev1correct").attr("src","sounds/level1/"+lan+"/"+ansr).trigger('play');  
			ansdes=jlev1[page][lan+"des"];
			$("#qustdiv1").text(ansdes).css({"text-align":"center","font-weight":"bold","font-size":"3vh","padding-top":"2vh","color":"green"});
			page+=1;
			right+=1;
			
			$(".descdiv").prop("disabled", true);			
		}else{
			$(this).removeClass("btn-warning").addClass("btn-danger");
			$("#wrng").trigger('play');
			wrong+=1;
			$(this).prop("disabled", true);
		}
		$("#rightscore").text(right);
		$("#wrongscore").text(wrong);	

	});

	$("#levsound").on('ended', function()   
	{
		$("#qustn").trigger('play');
	});

	$(".descvolimg").click(function(){
	
		opname=$(this).data("name");
		$("#dessound").attr("src","sounds/level1/"+lan+"/"+opname+".ogg").trigger('play');
	});

	$("#qusvolimg1").click(function(){

		$("#qustn").trigger('play');
	});

	$("#levelsound").click(function(){
		$("#levsound").trigger('play');
	});
		

	$("#lev1correct").on('ended', function()   
		{
			
			
		  	if(page<jlev1.length){
				$(".imageslev2,.optdesc").hide();
				displevel1(page);
				$("#qustn").trigger('play');
			}else{
				$("#cmplt").trigger('play');
				accuracy=Math.round((right/(right+wrong))*100);
					if(lan=="eng")
						alert("CONGRATULATIONS!! your accuracy is "+accuracy+"%");
					else
						alert("അഭിനന്തനങ്ങൾ !! നിങ്ങളുടെ അക്കുരസി "+accuracy+"%");
				reset();
			}
		});

		



		//lev2
	
	function displev2(page)
	{	
		$(".lev2descdiv").prop("disabled", false);
		$("#qustnlev2").fadeIn();
		$(".lev2descdiv").removeClass("btn-danger btn-success").addClass("btn-warning");
		$("#quesscore").text((page+1)+"/"+jlev2.length);
		qus=jlev2[page][lan+"question"];
		$("#qustdivlev2").text(qus).css({"text-align":"center","font-weight":"bold","font-size":"3vh","padding-top":"3vh","color":"black"});
		ques=jlev2[page]["sound"]+".ogg";
		$("#qustn").attr("src","sounds/level2/"+lan+"/"+ques);  
		$("#qustn").trigger('load');
		
		
		lev2order.sort(function() {return 0.5 - Math.random()});
		
		for(i=1;i<=3;i++)
		{
			order=lev2order[i-1];
			 $("#lev2optimg"+i).attr('src',"images/level2/"+jlev2[page].engoptns[order]+".jpg");
			lev2des=lan+"optns";
			$("#lev2descvolimg"+i).data("name",jlev2[page].engoptns[order]);
			$("#lev2descdiv"+i).text(jlev2[page][lev2des][order]).css({"text-align":"center","font-weight":"bold","font-size":"3vh","padding-top":"2vh"});
		}
		$(".imageslev2,.lev2optn").fadeIn();
		
	}
	
	$(".lev2descdiv").click(function(){
		
		if(($(this).text()==(jlev2[page][lan+"answer1"]))||($(this).text()==(jlev2[page][lan+"answer2"])))
		{
			$("#qustn").trigger("pause");
			$(this).removeClass("btn-warning").addClass("btn-success");
			$("#corct").trigger('play');
			ansnum+=1;
			right+=1;
			$("#rightscore").text(right);
			$(this).prop("disabled", true);	
		}else{
			$(this).removeClass("btn-warning").addClass("btn-danger");
			$("#wrng").trigger('play');
			wrong+=1;
			$(this).prop("disabled", true);
			$("#wrongscore").text(wrong);
		}
		
		
		if(ansnum==jlev2[page]["answernum"])
		{
			
			corctsound="correct"+soundno[page]+".ogg";
			$("#lev2correct").attr("src","sounds/"+corctsound).trigger('play'); 
			ansnum=0;
			page+=1;
			$(".lev2descdiv").prop("disabled", true);
		}		

	});

	$("#levsound").on('ended', function()   
	{
		$("#qustn").trigger('play');
	});

	$("#qusvolimg2").click(function(){
		$("#qustn").trigger('play');
	});

	$("#levelsound").click(function(){
		$("#levsound").trigger('play');
	});

	$(".lev2vol").click(function(){
	
		opname=$(this).data("name");
		$("#dessound").attr("src","sounds/level2/"+lan+"/"+opname+".ogg").trigger('play');
		
	});

		
		
	$("#lev2correct").on('ended', function()   
		{
			
			
		  	if(page<jlev2.length){

				$("#qustnlev2,.imageslev2,.lev2optn").hide();
				displev2(page);
				$("#qustn").trigger('play');

			}else{
				$("#cmplt").trigger('play');
				accuracy=Math.round((right/(right+wrong))*100);
					if(lan=="eng")
						alert("CONGRATULATIONS!! your accuracy is "+accuracy+"%");
					else
						alert("അഭിനന്തനങ്ങൾ !! നിങ്ങളുടെ അക്കുരസി "+accuracy+"%");
				reset();
			}
		});


});
