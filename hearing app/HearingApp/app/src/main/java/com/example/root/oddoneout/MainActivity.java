package com.example.root.oddoneout;

import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.transition.AutoTransition;
import android.support.transition.Scene;
import android.support.transition.Transition;
import android.support.transition.TransitionManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;

import java.io.IOException;
import java.util.ArrayList;

//import android.transition.TransitionInflater;

//import android.transition.Scene;
//import android.transition.Transition;
//import android.transition.TransitionInflater;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ViewGroup rootContainer;
    private Scene scene1, scene2, scene3,scene4,scene5,scene6,scene7,scene8,scene9,scene10,scene11,scene12,scene13,scene14,scene15;
    private TransitionManager transitionMgr;
    private Transition transition;
    private static final String TAG = "MainActivity";


    int [] instructionVoice;
    private MediaPlayer rightVoice, wrongVoice,currentVoice;
    MediaPlayer mediaPlayer;
    private int currentScene;
    int voice_index = 0;

    private ArrayList<Scene> list ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scene_transition);

        list = new ArrayList<Scene>();


        //Preparing a root container
        rootContainer = (ViewGroup) findViewById(R.id.rootContainer);


        //referencing scenes created in xml
        scene1 = Scene.getSceneForLayout(rootContainer, R.layout.scene1_layout, this);
        scene2 = Scene.getSceneForLayout(rootContainer, R.layout.scene4_layout, this);
        scene3 = Scene.getSceneForLayout(rootContainer, R.layout.alphabet_scene2_layout, this);
        scene4 = Scene.getSceneForLayout(rootContainer, R.layout.animal_scene1_layout, this);
        scene5 = Scene.getSceneForLayout(rootContainer, R.layout.vehicle_scene_layout, this);
        scene6 = Scene.getSceneForLayout(rootContainer, R.layout.fruit_scene_layout, this);
        scene7 = Scene.getSceneForLayout(rootContainer, R.layout.scene6_layout, this);
        scene8 = Scene.getSceneForLayout(rootContainer, R.layout.tool_scene_layout, this);
        scene9 = Scene.getSceneForLayout(rootContainer, R.layout.animal_scene2_layout, this);
        scene10 = Scene.getSceneForLayout(rootContainer, R.layout.furniture_scene_layout, this);
        scene11 = Scene.getSceneForLayout(rootContainer, R.layout.alphabet_scene6_layout, this);
        scene12 = Scene.getSceneForLayout(rootContainer, R.layout.bird_scene_layout, this);
        scene13 = Scene.getSceneForLayout(rootContainer, R.layout.transportation_scene_layout, this);
        scene14 = Scene.getSceneForLayout(rootContainer, R.layout.animal_scene3_layout, this);
        scene15 = Scene.getSceneForLayout(rootContainer, R.layout.scene11_layout, this);



        list.add(scene1);
        list.add(scene2);
        list.add(scene3);
        list.add(scene4);
        list.add(scene5);
        list.add(scene6);
        list.add(scene7);
        list.add(scene8);
        list.add(scene9);
        list.add(scene10);
        list.add(scene11);
        list.add(scene12);
        list.add(scene13);
        list.add(scene14);
        list.add(scene15);


        transition = new AutoTransition();
        transition.setDuration(500);
        transition.setInterpolator(new LinearInterpolator());

        setCurrentScene();
        //entering scene1
        scene1.enter();


        rightVoice = MediaPlayer.create(this, R.raw.correct);
        wrongVoice = MediaPlayer.create(this, R.raw.wrong);
        instructionVoice= new int[] {R.raw.no3,R.raw.alpha_q,R.raw.cow,R.raw.bus,R.raw.banana, R.raw.no8, R.raw.pencil, R.raw.camel, R.raw.table, R.raw.alpha_s, R.raw.chicken, R.raw.ship, R.raw.fish,  R.raw.claps};

        mediaPlayer = MediaPlayer.create(this, instructionVoice[0]);


    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.play:
               if(currentScene==1) {

                    changeToScene(scene2);
                  // voice_index = (voice_index +1);
                  // instructionVoice = MediaPlayer.create(this, R.raw.no3);
                   delayFunction();
                }
                else {
                    wrongVoice.start();
                }

                break;

            case R.id.ibno3:
                if(currentScene==2) {

                    changeToScene(scene3);
                    voice_index = (voice_index +1)%14;
                    //currentVoice = instructionVoice;
                   // instructionVoice = MediaPlayer.create(this, R.raw.alpha_d);
                    delayFunction();
                }
                else {
                    wrongVoice.start();

                }
                break;

            case R.id.alphaQ:

                if(currentScene==3) {

                    changeToScene(scene4);
                    voice_index = (voice_index +1)%14;
                    //currentVoice = instructionVoice;
                    //instructionVoice = MediaPlayer.create(this, R.raw.cow);
                    delayFunction();
                }
                else {
                    wrongVoice.start();
                }
                break;

            case R.id.cow:
                if(currentScene==4) {

                    changeToScene(scene5);
                    voice_index = (voice_index +1)%14;
                    //currentVoice = instructionVoice;
                    //instructionVoice = MediaPlayer.create(this, R.raw.bus);
                    delayFunction();
                }
                else {
                    wrongVoice.start();

                }
                break;

            case R.id.bus:
                if(currentScene==5) {

                    changeToScene(scene6);
                    voice_index = (voice_index +1)%14;
                   // currentVoice = instructionVoice;
                   // instructionVoice = MediaPlayer.create(this, R.raw.banana);
                    delayFunction();
                }
                else {
                    wrongVoice.start();


                }
                break;

            case R.id.banana:
                if(currentScene==6) {

                    changeToScene(scene7);
                    voice_index = (voice_index +1)%14;
                   // currentVoice = instructionVoice;
                    //instructionVoice = MediaPlayer.create(this, R.raw.no8);
                    delayFunction();
                }
                else {
                    wrongVoice.start();

                }
                break;

            case R.id.ibno8:
                if(currentScene==7) {

                    changeToScene(scene8);
                    voice_index = (voice_index +1)%14;
                   // currentVoice = instructionVoice;
                   // instructionVoice = MediaPlayer.create(this, R.raw.pencil);
                    delayFunction();
                }
                else {
                    wrongVoice.start();

                }
                break;

            case R.id.pencil:
                if(currentScene==8) {

                    changeToScene(scene9);
                    voice_index = (voice_index +1)%14;
                    //currentVoice = instructionVoice;
                    //instructionVoice = MediaPlayer.create(this, R.raw.camel);
                    delayFunction();
                }
                else {
                    wrongVoice.start();

                }
                break;

            case R.id.camel:
                if(currentScene==9) {

                    changeToScene(scene10);
                    voice_index = (voice_index +1)%14;
                    //currentVoice = instructionVoice;
                    //instructionVoice = MediaPlayer.create(this, R.raw.table);
                    delayFunction();

                }
                else {
                    wrongVoice.start();

                }
                break;

            case R.id.table:
                if(currentScene==10) {

                    changeToScene(scene11);
                    voice_index = (voice_index +1)%14;
                   // currentVoice = instructionVoice;
                    //instructionVoice = MediaPlayer.create(this, R.raw.alpha_s);
                    delayFunction();
                }
                else {
                    wrongVoice.start();

                }
                break;

            case R.id.alphaS:
                if(currentScene==11) {

                    changeToScene(scene12);
                    voice_index = (voice_index +1)%14;
                   // currentVoice = instructionVoice;
                  //  instructionVoice = MediaPlayer.create(this, R.raw.chicken);
                    delayFunction();
                }
                else {
                    wrongVoice.start();

                }
                break;
            case R.id.chicken:
                if(currentScene==12) {

                    changeToScene(scene13);
                    voice_index = (voice_index +1)%14;
                    //currentVoice = instructionVoice;
                    //instructionVoice = MediaPlayer.create(this, R.raw.ship);
                    delayFunction();
                }
                else {
                    wrongVoice.start();

                }
                break;

            case R.id.ship:
                if(currentScene==13) {

                    changeToScene(scene14);
                    voice_index = (voice_index +1)%14;
                    //currentVoice = instructionVoice;
                   // instructionVoice = MediaPlayer.create(this, R.raw.fish);
                    delayFunction();
                }
                else {
                    wrongVoice.start();

                }
                break;

            case R.id.fish:
                if(currentScene==14) {


                    changeToScene(scene15);
                    voice_index = (voice_index +1)%14;
                    //currentVoice = instructionVoice;
                   //instructionVoice = MediaPlayer.create(this, R.raw.claps);
                    delayFunction();
                }
                else {
                    wrongVoice.start();

                }
                break;





            case R.id.imageButton_inback:

                backToScene();
                break;

            case R.id.ib_spk:

                voice();
                break;
            default:
                wrongVoice.start();

        }


    }

    private void setCurrentScene() {
        scene1.setEnterAction(new Runnable() {
            @Override
            public void run() {
                currentScene = 1;

            }
        });
        scene2.setEnterAction(new Runnable() {
            @Override
            public void run() {
                currentScene = 2;
            }
        });
        scene3.setEnterAction(new Runnable() {
            @Override
            public void run() {
                currentScene = 3;
            }
        });
        scene4.setEnterAction(new Runnable() {
            @Override
            public void run() {
                currentScene = 4;
            }
        });
        scene5.setEnterAction(new Runnable() {
            @Override
            public void run() {
                currentScene = 5;

            }
        });
        scene6.setEnterAction(new Runnable() {
            @Override
            public void run() {
                currentScene = 6;

            }
        });
        scene7.setEnterAction(new Runnable() {
            @Override
            public void run() {
                currentScene = 7;

            }
        });
        scene8.setEnterAction(new Runnable() {
            @Override
            public void run() {
                currentScene = 8;

            }
        });
        scene9.setEnterAction(new Runnable() {
            @Override
            public void run() {
                currentScene = 9;

            }
        });
        scene10.setEnterAction(new Runnable() {
            @Override
            public void run() {
                currentScene = 10;


            }
        });
        scene11.setEnterAction(new Runnable() {
            @Override
            public void run() {
                currentScene = 11;


            }
        });
        scene12.setEnterAction(new Runnable() {
            @Override
            public void run() {
                currentScene = 12;


            }
        }); scene13.setEnterAction(new Runnable() {
            @Override
            public void run() {
                currentScene = 13;


            }
        }); scene14.setEnterAction(new Runnable() {
            @Override
            public void run() {
                currentScene = 14;



            }
        });
        scene15.setEnterAction(new Runnable() {
            @Override
            public void run() {
                currentScene = 15;


            }
        });

    }


    private void changeToScene(final Scene scene) {


        rightVoice.start();
        rightVoice.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {

                TransitionManager.go(scene, transition);
            }

        });

    }

    private void backToScene() {

        int index= currentScene-2;
        voice_index = (voice_index -1)%14;
        TransitionManager.go(list.get(index),transition);


    }
private void voice(){

    AssetFileDescriptor afd = this.getResources().openRawResourceFd(instructionVoice[voice_index]);

   // instructionVoice.start();
    try
    {
        mediaPlayer.reset();
        mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getDeclaredLength());
        mediaPlayer.prepare();
        mediaPlayer.start();
        afd.close();
    }
    catch (IllegalArgumentException e)
    {
        Log.e(TAG, "Unable to play audio queue do to exception: " + e.getMessage(), e);
    }
    catch (IllegalStateException e)
    {
        Log.e(TAG, "Unable to play audio queue do to exception: " + e.getMessage(), e);
    }
    catch (IOException e)
    {
        Log.e(TAG, "Unable to play audio queue do to exception: " + e.getMessage(), e);
    }

}

    public void delayFunction(){
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

               voice();
            }
        }, 3000);
    }
}

